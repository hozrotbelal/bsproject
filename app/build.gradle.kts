plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id ("com.google.devtools.ksp")
    id ("dagger.hilt.android.plugin")

}


android {
    namespace = "com.example.gitrepoapp"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.example.gitrepoapp"
        minSdk = 23
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }


    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }

        getByName("debug") {
            applicationIdSuffix = ".debug"
            isDebuggable = true
        }
    }

//    flavorDimensions += "version"
//    productFlavors {
//        create("demo") {
//            buildConfigField("String","API_SUB_DOMAIN","\"demo\"")
//            applicationIdSuffix = ".demo"
//            versionNameSuffix = "-demo"
//            dimension = "version"
//        }
//        create("dev") {
//            buildConfigField ("String", "API_SUB_DOMAIN", "\"dev\"")
//            applicationIdSuffix = ".dev"
//            versionNameSuffix = "-dev"
//            dimension = "version"
//
//        }
//
//    }

    compileOptions {

        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        compose = true
        viewBinding  = true
        buildConfig = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.5.1"
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }

}

dependencies {
    // Core
    implementation("androidx.core:core-ktx:1.12.0")
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.6.2")
    implementation("androidx.activity:activity-compose:1.8.2")
    implementation(platform("androidx.compose:compose-bom:2023.08.00"))
    implementation("androidx.compose.ui:ui")
    implementation("androidx.compose.ui:ui-graphics")
    implementation("androidx.compose.ui:ui-tooling-preview")
    implementation ("androidx.appcompat:appcompat:1.6.1")
    implementation ("org.jetbrains.kotlin:kotlin-reflect:1.9.0")
    //implementation("androidx.compose.material3:material3")
    implementation ("androidx.compose.material:material:1.5.4")

    //test
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
    androidTestImplementation(platform("androidx.compose:compose-bom:2023.08.00"))
    androidTestImplementation("androidx.compose.ui:ui-test-junit4")
    debugImplementation("androidx.compose.ui:ui-tooling")
    debugImplementation("androidx.compose.ui:ui-test-manifest")

    // Navigation
    implementation ("androidx.navigation:navigation-compose:2.7.6")
    implementation ("io.github.raamcosta.compose-destinations:animations-core:1.9.55")
    ksp ("io.github.raamcosta.compose-destinations:ksp:1.9.55")
    implementation ("androidx.constraintlayout:constraintlayout:2.1.4")

    //API
    implementation ("com.squareup.retrofit2:retrofit:2.9.0")
    implementation ("com.squareup.retrofit2:converter-gson:2.9.0")
    implementation ("com.squareup.okhttp3:okhttp:5.0.0-alpha.10")
    implementation ("com.squareup.okhttp3:logging-interceptor:5.0.0-alpha.10")
    // View model, Livedata
    val lifecycleVersion = "2.6.2"
    implementation ("androidx.lifecycle:lifecycle-viewmodel-ktx:$lifecycleVersion")
    implementation ("androidx.lifecycle:lifecycle-viewmodel-compose:$lifecycleVersion")
    implementation ("androidx.lifecycle:lifecycle-livedata-ktx:$lifecycleVersion")
    implementation ("androidx.lifecycle:lifecycle-runtime-ktx:$lifecycleVersion")
    implementation ("androidx.lifecycle:lifecycle-viewmodel-savedstate:$lifecycleVersion")

    //Room
    val roomVersion = "2.6.1"
    implementation("androidx.room:room-runtime:$roomVersion")
    implementation("androidx.room:room-ktx:$roomVersion")
    annotationProcessor("androidx.room:room-compiler:$roomVersion")
    ksp ("androidx.room:room-compiler:$roomVersion")

    //Data Store
    implementation ("androidx.datastore:datastore-preferences:1.0.0")

//    // DI
    implementation ("com.google.dagger:hilt-android:2.50")
    ksp ("com.google.dagger:hilt-android-compiler:2.50")
    implementation ("androidx.hilt:hilt-navigation-compose:1.2.0-alpha01")

    implementation ("androidx.hilt:hilt-work:1.1.0")

    //Work manager
    val work_version = "2.9.0"
    implementation ("androidx.work:work-runtime-ktx:$work_version")

    // Accompanist
    implementation ("com.google.accompanist:accompanist-pager:0.25.0")
    implementation ("com.google.accompanist:accompanist-pager-indicators:0.25.0")
    implementation ("com.google.accompanist:accompanist-systemuicontroller:0.30.1")
    implementation ("com.google.accompanist:accompanist-permissions:0.33.2-alpha")
    implementation ("com.google.accompanist:accompanist-swiperefresh:0.28.0")
    implementation ("com.google.accompanist:accompanist-flowlayout:0.33.2-alpha")

    // View
    implementation ("androidx.core:core-splashscreen:1.0.1")
    implementation ("io.coil-kt:coil-compose:2.4.0")
    implementation ("io.coil-kt:coil-svg:2.4.0")
    implementation ("com.kizitonwose.calendar:compose:2.4.0")

    // Util
    coreLibraryDesugaring ("com.android.tools:desugar_jdk_libs:2.0.4")
    implementation ("com.orhanobut:logger:2.2.0")
//    implementation ("com.github.GrenderG:Toasty:1.5.2")
    implementation ("com.auth0.android:jwtdecode:2.0.2")
    implementation ("com.blankj:utilcodex:1.31.1")
   // implementation ("com.github.commandiron:WheelPickerCompose:1.1.10")
}