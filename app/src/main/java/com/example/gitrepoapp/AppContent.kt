package com.example.gitrepoapp

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.*
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import com.example.gitrepoapp.presentation.NavGraphs
import com.example.gitrepoapp.presentation.bottom_nav.NavBottomBar


import com.google.accompanist.navigation.material.ExperimentalMaterialNavigationApi
import com.ramcosta.composedestinations.DestinationsNavHost
import com.ramcosta.composedestinations.animations.defaults.NestedNavGraphDefaultAnimations
import com.ramcosta.composedestinations.animations.defaults.RootNavGraphDefaultAnimations
import com.ramcosta.composedestinations.animations.rememberAnimatedNavHostEngine


@OptIn(ExperimentalMaterialNavigationApi::class, ExperimentalAnimationApi::class)
@Composable
fun AppContent(navController: NavHostController) {
//
    val engine = rememberAnimatedNavHostEngine(
        navHostContentAlignment = Alignment.TopCenter,
        rootDefaultAnimations = RootNavGraphDefaultAnimations.ACCOMPANIST_FADING,
        defaultAnimationsForNestedNavGraph = mapOf(
            NavGraphs.root to NestedNavGraphDefaultAnimations.ACCOMPANIST_FADING,
            NavGraphs.user to NestedNavGraphDefaultAnimations.ACCOMPANIST_FADING,
        )
    )

    Scaffold(
        bottomBar = {
            NavBottomBar(navController = navController)

        }) { padding ->

        Box(modifier = Modifier.padding(padding)) {
            DestinationsNavHost(
                navGraph = NavGraphs.root,
                navController = navController,
                engine = engine,
                dependenciesContainerBuilder = {

                    val parentEntry = remember(navBackStackEntry) {
                        navController.getBackStackEntry(NavGraphs.root.route)
                    }


                    // Employee

//                    dependency(NavGraphs.user) {
//                        remember(navBackStackEntry) {
//                            navController.getBackStackEntry(NavGraphs.user.route)
//                        }.let { hiltViewModel<BSUserListVM>(it) }
//                    }

                  }
            )

        }

    }
}

