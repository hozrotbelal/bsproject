package com.example.gitrepoapp

import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.ui.Modifier
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.core.view.WindowCompat
import androidx.lifecycle.lifecycleScope
import androidx.navigation.compose.rememberNavController
import com.example.gitrepoapp.presentation.ui.theme.TestComposeAppTheme

import com.example.gitrepoapp.util.SetStatusBarColor
import com.example.gitrepoapp.util.SetNavigationBarColor

import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            val splashScreen = installSplashScreen()
            lifecycleScope.launch {
                splashScreen.setKeepOnScreenCondition { true }
                delay(1500)
                splashScreen.setKeepOnScreenCondition { false }
            }
        }
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, false)

        setContent {
            val darkTheme = false
            SetStatusBarColor(useDarkIcons = darkTheme)
            SetNavigationBarColor(useDarkIcons = darkTheme)

            val navController = rememberNavController()

            TestComposeAppTheme(darkTheme = darkTheme) {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    AppContent(navController)
                }
            }

        }

    }

}
