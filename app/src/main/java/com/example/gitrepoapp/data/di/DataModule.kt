package com.example.gitrepoapp.data.di

import android.content.Context

import com.example.gitrepoapp.data.local.dataStore.DataStoreRepository
import com.example.gitrepoapp.data.local.dataStore.DataStoreRepositoryImpl


import com.example.gitrepoapp.data.remote.api_service.*
import com.example.gitrepoapp.data.remote.common.ApiUrlConst

import com.example.gitrepoapp.util.CacheInterceptor
import com.example.gitrepoapp.util.ETagInterceptor
import com.example.gitrepoapp.util.HeaderInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object DataModule {

    /* DataStore */

    @Singleton
    @Provides
    fun provideDataStoreRepository(
        @ApplicationContext context: Context
    ): DataStoreRepository = DataStoreRepositoryImpl(context)

    /* GraphQL */


    /* Room */

//    @Provides
//    @Singleton
//    fun provideRoomDb(@ApplicationContext context: Context) =
//        Room.databaseBuilder(
//            context,
//            AppDatabase::class.java, "database-name"
//        )
//            .fallbackToDestructiveMigration()
//            .build()


    /* Retrofit */

    @Provides
    @Singleton
    fun provideCache(@ApplicationContext context: Context): Cache {
//        val httpCacheDirectory = File(context.cacheDir, "offlineCache")
        val cache = Cache(context.cacheDir, 10L * 1024L * 1024L)
        return cache
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(
        headerInterceptor: HeaderInterceptor,
        cache: Cache,
//        cacheLoggingInterceptor: CacheLoggingInterceptor
        cacheInterceptor: CacheInterceptor,
        eTagInterceptor: ETagInterceptor,
//        forceCacheInterceptor: ForceCacheInterceptor,
//        authenticatorForRetrofit: AuthenticatorForRetrofit
    ) = OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        .addInterceptor(headerInterceptor)
//        .addNetworkInterceptor(cacheLoggingInterceptor)
//        .authenticator(authenticatorForRetrofit)
//        .addNetworkInterceptor(eTagInterceptor)
        .addInterceptor(cacheInterceptor)
//        .addInterceptor(forceCacheInterceptor)
        .connectTimeout(30, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS)
        .cache(cache)
        .build()

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {

//        okHttpClient.cache?.evictAll()

        return Retrofit.Builder()
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(ApiUrlConst.BASE_URL)
            .build()
    }


    /* Api Services */

    @Provides
    @Singleton
    fun provideUserApi(retrofit: Retrofit): GetSearchApiService =
        retrofit.create(GetSearchApiService::class.java)

}
