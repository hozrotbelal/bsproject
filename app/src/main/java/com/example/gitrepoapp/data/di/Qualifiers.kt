package com.app.alignwellness.data.di

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class ApolloClientHealthQuery

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class ApolloClientHealth

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class ApolloClientMoodQuery

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class ApolloClientOrganizationQuery