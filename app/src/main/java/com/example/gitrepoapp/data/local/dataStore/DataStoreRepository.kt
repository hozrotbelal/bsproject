package com.example.gitrepoapp.data.local.dataStore

interface DataStoreRepository {

    suspend fun getString(key: String): String?
    suspend fun putString(key: String, value: String)
    suspend fun delete(key: String)

}