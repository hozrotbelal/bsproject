package com.example.gitrepoapp.data.remote.api_service

import com.example.gitrepoapp.data.remote.dto.*

import com.example.gitrepoapp.data.remote.dto.ApiResponse
import com.example.gitrepoapp.data.remote.dto.search.PayloadItem
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query


interface GetSearchApiService {

    @GET("/search/repositories")
    suspend fun getGitSearchRepositories(
        @Query(value = "q") q: String?,
        @Query(value = "sort") sort: String?,
        @Query(value = "order") order: String?,
        @Query(value = "per_page") perPage: Int?,
        @Query(value = "page") page: Int?,
    ): Response<ApiResponse<List<PayloadItem>>>

}