package com.example.gitrepoapp.data.remote.common

import com.app.alignwellness.data.remote.common.errorListToString
import com.example.gitrepoapp.data.remote.dto.ApiResponse
import com.example.gitrepoapp.util.ApiResult
import com.orhanobut.logger.Logger
import org.json.JSONException
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException
import java.net.SocketTimeoutException

suspend fun <T> apiCall(requestFunc: suspend () -> Response<ApiResponse<T>>): ApiResult<T> {
    return try {
        val response = requestFunc.invoke()
        if (response.isSuccessful) {
            ApiResult.Success(
                payload = response.body()?.payload,
                data = response.body()?.data,
                body = response.body(),
                requestResponse = response.raw()
            )
        } else {

            val errorBody = response.errorBody()
            val errorMessage = when {
                errorBody != null -> errorListToString(errorBody, code = response.code())
                response.code() == 401 -> "Unauthorized"
                else -> "Request failed with code: ${response.code()}"
            }
            ApiResult.Error(message = errorMessage)
        }

    } catch (e: Exception) {
        Logger.d(e::class)
        e.printStackTrace()
        when (e) {
            is HttpException ->
                ApiResult.Error(
                    message = errorListToString(e.response()?.errorBody(), code = e.code())
                )

            is java.lang.ClassCastException -> ApiResult.Error("ClassCastException")
            is SocketTimeoutException -> ApiResult.Error("Timeout")
            is IOException -> ApiResult.Error("Check internet connection")
            is JSONException -> ApiResult.Error("Gateway Time-out")
            else -> ApiResult.Error("Unknown network error")
        }
    }
}