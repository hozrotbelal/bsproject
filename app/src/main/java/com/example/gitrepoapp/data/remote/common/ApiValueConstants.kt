package com.example.gitrepoapp.data.remote.common

enum class EnumIdentityGrandType {
    authenticate_site,
    refresh_token,
    password,
    password_direct,
    two_fa_login,
}


enum class EnumArithmeticalSymbol(val value: Int, val textForShow: String) {
    EqualTo(1, "=="),
    LessThan(2, "<"),
    GreaterThan(3, ">"),
    LessThanOrEqualTo(4, "<="),
    GreaterThanOrEqualTo(5, ">="),
    Range(6, "~");

    companion object {
        fun find(value: Int): EnumArithmeticalSymbol? {
            return EnumArithmeticalSymbol.values().firstOrNull { it.value == value }
        }
    }
}
