package com.app.alignwellness.data.remote.common

import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception

fun errorListToString(errorBody: ResponseBody?, code: Int): String {
    try {
        if (errorBody != null) {
            val jsonObj = JSONObject(errorBody.string())
            val errorArray: JSONArray = jsonObj.get("errors") as JSONArray

            val a = StringBuilder()

            for (i in 0 until errorArray.length()) {
                val errorObj: JSONObject = errorArray.getJSONObject(i) as JSONObject
                val messages: JSONArray = errorObj.get("message") as JSONArray
                for (j in 0 until messages.length()) {
                    if (j != 0) a.append("\n")
                    a.append(messages.get(j))
                }
            }
            return a.toString()
        } else {
            return "$code: Error body empty."
        }
    } catch (e: Exception) {
        e.printStackTrace()
        return "$code: Data parsing error."
    }

}