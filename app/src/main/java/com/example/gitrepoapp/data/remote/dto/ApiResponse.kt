package com.example.gitrepoapp.data.remote.dto

import com.google.gson.annotations.SerializedName


data class ApiResponse<P>(
    @SerializedName("errors")
    val errors: List<Error?>? = null,
    @SerializedName("total_count")
    val totalCount: Int? = null,
    @SerializedName("httpStatusCode")
    val httpStatusCode: Int? = null,
    @SerializedName("isValidFailed")
    val isValidFailed: Boolean? = null,
    @SerializedName("message")
    val message: String? = null,
    @SerializedName("items")
    val payload: P? = null,
    @SerializedName("data")
    val data: P? = null,
  )

data class Error(
    @SerializedName("field")
    val `field`: String? = null,
    @SerializedName("message")
    val message: List<String>? = null
)

