package com.example.gitrepoapp.data.repository.search

import com.example.gitrepoapp.data.remote.api_service.GetSearchApiService
import com.example.gitrepoapp.data.remote.common.apiCall
import com.example.gitrepoapp.data.remote.dto.search.PayloadItem
import com.example.gitrepoapp.util.ApiResult
import com.example.gitrepoapp.util.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class SearchRepository @Inject constructor(
    private val employeeApiService: GetSearchApiService,
) {
    fun getGitSearchRepositories(
        q: String? = null,
        sort: String? = null,
        pageIndex: Int,
        perPage: Int = 50,
        order: String? = null,
    ): Flow<Resource<List<PayloadItem>>> {
        return flow {

            emit(Resource.Loading())
            val apiResult = apiCall {
                employeeApiService.getGitSearchRepositories(
                    q = q,
                    sort = sort,
                    order = order,
                    perPage = perPage,
                    page = pageIndex,
                )
            }

            emit(Resource.Loading(false))

            when (apiResult) {
                is ApiResult.Error -> emit(Resource.Error(apiResult.message))
                is ApiResult.Success -> emit(Resource.Success(apiResult.payload))
            }

        }
    }



}