package com.example.gitrepoapp.presentation.app_switcher

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.gitrepoapp.presentation.ui.theme.white
import com.orhanobut.logger.Logger
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.annotation.RootNavGraph
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import kotlinx.coroutines.flow.collectLatest
import com.example.gitrepoapp.R
import com.example.gitrepoapp.presentation.NavGraphs
import com.example.gitrepoapp.presentation.destinations.*



@RootNavGraph(start = true)
@Destination
@Composable
fun ApplicationSwitcher(navigator: DestinationsNavigator) {
    val vm = hiltViewModel<ApplicationSwitcherVM>()

    var errorMessage by remember { mutableStateOf("") }

    LaunchedEffect(key1 = Unit, block = {
        vm.eventChannel.collectLatest {
            when (it) {
                ApplicationSwitcherEvent.Loading -> {
                    Logger.d("Loading in ApplicationSwitcher")
                }
                ApplicationSwitcherEvent.OpenClientScreen -> {
                    navigator.navigate(BSUserListFragmentDestination) {
                        popUpTo(NavGraphs.root.route) { inclusive = true }
                    }
                }

                is ApplicationSwitcherEvent.InvalidNavigation -> {
                    errorMessage = it.message
                }
            }
        }
    })

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(color = MaterialTheme.colors.white),
        contentAlignment = Alignment.Center
    ) {
        if (errorMessage.isBlank()) {
            Image(
                painter = painterResource(id = R.drawable.ic_launcher_background),
                contentDescription = "",
                modifier = Modifier.size(250.dp)
            )
        } else {
            Text(text = errorMessage)
        }
    }
}
