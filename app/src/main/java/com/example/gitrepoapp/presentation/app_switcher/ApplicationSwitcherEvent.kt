package com.example.gitrepoapp.presentation.app_switcher

sealed class ApplicationSwitcherEvent {

    object OpenClientScreen : ApplicationSwitcherEvent()

    object Loading : ApplicationSwitcherEvent()

    class InvalidNavigation(val message: String) : ApplicationSwitcherEvent()
}