package com.example.gitrepoapp.presentation.app_switcher

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ApplicationSwitcherVM @Inject constructor(
) : ViewModel() {

    private val _eventChannel = Channel<ApplicationSwitcherEvent>()
    val eventChannel = _eventChannel.receiveAsFlow()

    init {
        viewModelScope.launch {
            _eventChannel.send(ApplicationSwitcherEvent.OpenClientScreen)

        }
    }

}