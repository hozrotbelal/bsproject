package com.example.gitrepoapp.presentation.bottom_nav

import androidx.annotation.DrawableRes
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.currentBackStackEntryAsState
import com.example.gitrepoapp.R
import com.example.gitrepoapp.presentation.NavGraphs
import com.example.gitrepoapp.presentation.destinations.*
import com.example.gitrepoapp.presentation.ui.theme.*
import com.example.gitrepoapp.util.coloredShadow
import com.ramcosta.composedestinations.spec.DirectionDestinationSpec


enum class BottomBarDestination(
    val direction: DirectionDestinationSpec,
    val label: String,
    @DrawableRes val icon: Int
) {
    Entries(BSUserListFragmentDestination, "Entries", R.drawable.ic_bottom_nav_entries),
    Stats(StarsScreenDestination, "Stats", R.drawable.ic_bottom_nav_stats),
    Create(BSUserListFragmentDestination, "Create", R.drawable.ic_bottom_nav_circle_plus),
    Calendar(CalendersScreenDestination, "Calendar", R.drawable.ic_bottom_nav_remainder),
    More(MoreScreenDestination, "More", R.drawable.ic_more_vertical),
}


@OptIn(ExperimentalLayoutApi::class)
@Composable
fun NavBottomBar(navController: NavController) {

    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentDestination = navBackStackEntry?.destination

    val isVisibleBottomNav = BottomBarDestination.values().any {
        it.direction.route == currentDestination?.route
    }


    val context = LocalContext.current


    AnimatedVisibility(
        visible = isVisibleBottomNav,
        enter = fadeIn() + slideInVertically(initialOffsetY = { it / 2 }),
        exit = slideOutVertically(targetOffsetY = { it / 2 }) + fadeOut(),
    ) {
        Box(modifier = Modifier) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .coloredShadow(
                        Color.Black,
                        borderRadius = 15.dp,
                        shadowRadius = 10.dp
                    )
                    .clip(
                        RoundedCornerShape(
                            topStart = 15.dp,
                            topEnd = 15.dp,
                            bottomStart = 0.dp,
                            bottomEnd = 0.dp
                        )
                    )
                    .background(color = MaterialTheme.colors.white)
                    .navigationBarsPadding()
                    .align(alignment = Alignment.BottomCenter)
                    .height(60.dp)
            ) {

                Row(
                    horizontalArrangement = Arrangement.SpaceEvenly,
                    modifier = Modifier.fillMaxSize(),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    BottomBarDestination.values().forEach {
//                    if (it.direction.route != MoodBottomBarDestination.Create.direction.route) {
                        Column(
                            verticalArrangement = Arrangement.spacedBy(6.dp),
                            horizontalAlignment = Alignment.CenterHorizontally,
                            modifier = Modifier
                                .weight(1f)
                                .clickable(
                                    indication = null,
                                    interactionSource = remember { MutableInteractionSource() }) {
                                    navController.navigate(it.direction.route) {
                                        popUpTo(NavGraphs.user.startRoute.route) {
                                            saveState = true
                                        }
                                        launchSingleTop = true
                                        restoreState = true
                                    }
                                }
                        ) {
                            Image(
                                painter = painterResource(id = it.icon),
                                contentDescription = "",
                                colorFilter = if (currentDestination?.route == it.direction.route) ColorFilter.tint(
                                    if (it.toString() == BottomBarDestination.Create.label) MaterialTheme.colors.transparent else MaterialTheme.colors.orange
                                ) else ColorFilter.tint(
                                    if (it.toString() == BottomBarDestination.Create.label) MaterialTheme.colors.white else MaterialTheme.colors.textGray4E
                                )
                            )
                            Text(
                                text = if (it.toString() != BottomBarDestination.Create.label) it.label else "",
                                style = MaterialTheme.typography.s11w400.copy(color = if (currentDestination?.route == it.direction.route) MaterialTheme.colors.orange else MaterialTheme.colors.textGray4E),
                            )
                        }

                    }
                }


            }

            if (isVisibleBottomNav) Box(

                modifier = Modifier
                    .navigationBarsPadding()
                    .padding(bottom = 30.dp)
                    .background(
                        color = MaterialTheme.colors.orange,
                        shape = CircleShape
                    )
                    .align(alignment = Alignment.BottomCenter)
                    .size(50.dp)
                    .clickable {

                    }
            ) {
                Image(
                    painter = painterResource(id = R.drawable.ic_plus),
                    contentDescription = "lfn",
                    colorFilter = ColorFilter.tint(color = MaterialTheme.colors.white),
                    modifier = Modifier
                        .align(Alignment.Center)
                )
            }

        }

    }


}