package com.example.gitrepoapp.presentation.common_event

sealed class CommonEvent {

    class ShowMessage(val message: String, val isError: Boolean = true) : CommonEvent()

    object NavigateNext : CommonEvent()
}


