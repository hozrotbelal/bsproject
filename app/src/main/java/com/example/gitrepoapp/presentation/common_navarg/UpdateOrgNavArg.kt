package com.example.gitrepoapp.presentation.common_navarg

import com.example.gitrepoapp.data.remote.dto.search.PayloadItem


data class UpdateItemNavArg(val obj: PayloadItem?)