package com.example.gitrepoapp.presentation.common_screen

import android.app.Activity
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp

import com.example.gitrepoapp.presentation.ui.theme.s18w700
import com.example.gitrepoapp.presentation.ui.theme.text71
import com.example.gitrepoapp.presentation.ui.theme.white
import com.example.gitrepoapp.presentation.ui.ui_components.CAppBar
import com.example.gitrepoapp.presentation.ui.ui_components.CButton
import com.ramcosta.composedestinations.annotation.Destination

@Destination
@Composable
fun CalendersScreen() {
    val context = LocalContext.current

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(color = MaterialTheme.colors.white)
            .clickable(enabled = false, onClick = { })
    ) {
        CAppBar(
            title = "Calender", showLeadingIcon = false,

        )
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center,
            modifier = Modifier.padding(top = 110.dp).align(Alignment.Center)
        ) {


            Spacer(modifier = Modifier.height(20.dp))

            Text(
                text = "Calender View",
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.s18w700.copy(color = MaterialTheme.colors.text71),
                modifier = Modifier
            )

            Spacer(modifier = Modifier.height(40.dp))

            CButton(text = "Okay", modifier = Modifier.width(150.dp)) {
                val activity = (context as? Activity)
                activity?.finish()
            }

        }
    }
}