package com.example.gitrepoapp.presentation.feature.details

import android.os.Build
import androidx.compose.foundation.Image
import androidx.compose.foundation.background

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel

import com.example.gitrepoapp.presentation.ui.theme.text71

import com.example.gitrepoapp.presentation.ui.theme.white

import com.example.gitrepoapp.util.keyboardAsState
import com.example.gitrepoapp.R
import com.example.gitrepoapp.data.remote.dto.search.PayloadItem
import com.example.gitrepoapp.presentation.feature.search.GitSearchListVM
import com.example.gitrepoapp.presentation.ui.theme.bgSolitude6FF
import com.example.gitrepoapp.presentation.ui.theme.s12w500
import com.example.gitrepoapp.presentation.ui.theme.s13w500
import com.example.gitrepoapp.presentation.ui.theme.s14w700
import com.example.gitrepoapp.presentation.ui.theme.s18w700
import com.example.gitrepoapp.presentation.ui.theme.text5D
import com.example.gitrepoapp.presentation.ui.theme.text73
import com.example.gitrepoapp.presentation.ui.theme.textGray4E
import com.example.gitrepoapp.presentation.ui.ui_components.CAppBar
import com.example.gitrepoapp.presentation.ui.ui_components.CImage
import com.example.gitrepoapp.util.DTUtils
import com.example.gitrepoapp.util.UserNavGraph
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator

object SearchItem {
    var itemObj: PayloadItem? = null
   }

@Destination
@UserNavGraph
@Composable
fun SearchDetails(
    navigator: DestinationsNavigator? = null,
    //resultBackNavigator: ResultBackNavigator<Boolean>,
) {
    val vm = hiltViewModel<GitSearchListVM>()
    val state = vm.state.collectAsState()
    val keyBoard = LocalSoftwareKeyboardController.current
    val keyboardCheck by keyboardAsState()
    val scrollState = rememberScrollState()

    val context = LocalContext.current

    Box(
        modifier = Modifier
            .fillMaxSize()
        //.navigationBarsPadding()
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(color = MaterialTheme.colors.background)

        ) {
            CAppBar(
                showLeadingIcon = true,
                title = "Details",
                onLeadingIconTap = {
                 //   resultBackNavigator.navigateBack(result = state.value.isResultBack)
                   navigator?.popBackStack()
                }
            )
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    //.imePadding()
                    .verticalScroll(scrollState)
            ) {

            LazyColumn(
                    modifier = Modifier
                        .fillMaxWidth()
                        .weight(1.0f),
                    content = {
                        item {

                            Spacer(modifier = Modifier.height(10.dp))

                            Box(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .height(210.dp)
                            ) {
                                CImage(
                                    modifier = Modifier
                                        .padding(horizontal = 17.dp)
                                        .clip(RoundedCornerShape(12.dp))
                                        .fillMaxWidth()
                                        .height(170.dp)
                                        .clip(RectangleShape),
                                    model = "",//state.value.coverPhotoUrl,
                                    contentScale = ContentScale.Fit,
                                    error = {
                                        Image(
                                            painter = painterResource(id = R.drawable.img_banner_placeholder),
                                            contentDescription = "errorImage"
                                        )
                                    }
                                )

                                Box(
                                    modifier = Modifier
                                        .size(80.dp)
                                        .align(Alignment.BottomCenter)
                                        .clip(CircleShape)
                                        .background(color = MaterialTheme.colors.bgSolitude6FF)
                                    // .padding(10.dp)
                                ) {

                                    CImage(
                                        modifier = Modifier
                                            .padding(2.dp)
                                            .size(80.dp)
                                            .clip(CircleShape),
                                        model =  if (SearchItem.itemObj != null) SearchItem.itemObj?.owner?.avatarUrl
                                            ?: "" else "",//state.value.logoUrl
                                    )
                                }


                            }
                        }

                        item {

                            Spacer(modifier = Modifier.height(20.dp))
                            Text(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(
                                        start = 17.dp,
                                        end = 16.dp
                                    ),
                                text = "Details Information",
                                style = MaterialTheme.typography.s18w700.copy(color = MaterialTheme.colors.textGray4E),
                                textAlign = TextAlign.Left
                            )
                            Column(
                                modifier = Modifier
                                    .padding(
                                        top = 7.dp,
                                        start = 17.dp,
                                        end = 16.dp
                                    )
                                    .clip(RoundedCornerShape(12.dp))
                                    .fillMaxWidth()
                                    .wrapContentHeight()
                                    .background(color = MaterialTheme.colors.white)
                                    .padding(
                                        start = 10.dp,
                                        end = 10.dp
                                    )

                            ) {

                                Spacer(modifier = Modifier.height(10.dp))

                                HVEventDetails(
                                    txtLeft = "Owner Name",
                                    leftLabelStyle = MaterialTheme.typography.s12w500.copy(color = MaterialTheme.colors.text5D),
                                    txtRight = if (SearchItem.itemObj != null) SearchItem.itemObj?.name
                                        ?: "" else "",
                                    rightLabelStyle = MaterialTheme.typography.s12w500.copy(color = MaterialTheme.colors.text5D),
                                )

                                HVEventDetails(
                                    txtLeft = "Description",
                                    leftLabelStyle = MaterialTheme.typography.s12w500.copy(color = MaterialTheme.colors.text71),
                                    txtRight =  if (SearchItem.itemObj != null) SearchItem.itemObj?.description
                                        ?: "" else "",
                                    rightLabelStyle = MaterialTheme.typography.s12w500.copy(color = MaterialTheme.colors.text71),
                                )

                                HVEventDetails(
                                    txtLeft = "Date",
                                    leftLabelStyle = MaterialTheme.typography.s12w500.copy(color = MaterialTheme.colors.text71),
                                    txtRight = if (SearchItem.itemObj != null) if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                        DTUtils.getLocalFormattedString(dateString = SearchItem.itemObj?.updatedAt)
                                            ?: ""
                                    } else {
                                        ""
                                    } else "",
                                    rightLabelStyle = MaterialTheme.typography.s12w500.copy(color = MaterialTheme.colors.text71),
                                )
                                Spacer(modifier = Modifier.height(10.dp))
                            }


                        }

                    })
            }
        }
    }
}


@Preview(showBackground = true)
@Composable
fun HVEventDetails(
    txtLeft: String = "",
    txtRight: String = "",
    txtLeftWeight: Float = .7f,
    txtRightWeight: Float = 1.0f,
    leftLabelStyle: TextStyle = MaterialTheme.typography.s14w700.copy(color = MaterialTheme.colors.text73),
    rightLabelStyle: TextStyle = MaterialTheme.typography.s13w500.copy(color = MaterialTheme.colors.text73),
    textAlignRight: TextAlign = TextAlign.Left,
) {

    Row(
        modifier = Modifier
            .padding(vertical = 6.dp)
            .fillMaxWidth(),
    ) {

        Text(
            text = txtLeft,
            modifier = Modifier
                .weight(txtLeftWeight),
            style = leftLabelStyle,
        )
        Spacer(modifier = Modifier.width(10.dp))
        Text(
            text = txtRight,
            textAlign = textAlignRight,
            modifier = Modifier
                .weight(txtRightWeight),
            style = rightLabelStyle
        )
    }
}
