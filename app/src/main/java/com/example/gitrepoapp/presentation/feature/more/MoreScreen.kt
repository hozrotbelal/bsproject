package com.example.gitrepoapp.presentation.feature.more

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import androidx.navigation.NavBackStackEntry

import com.example.gitrepoapp.R
import com.example.gitrepoapp.presentation.ui.theme.white
import com.example.gitrepoapp.presentation.ui.ui_components.CAppBar
import com.example.gitrepoapp.presentation.ui.ui_components.CImage
import com.example.gitrepoapp.presentation.ui.ui_components.CheckCircle
import com.example.gitrepoapp.presentation.ui.ui_components.ItemAccount
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator


@Destination
@Composable
fun MoreScreen(
    navBackStackEntry: NavBackStackEntry,
    navigator: DestinationsNavigator? = null,
   // resultBackNavigator: ResultBackNavigator<Boolean>,
) {



    Column {
        CAppBar(
            title = "More", showLeadingIcon = false,

        )
        LazyColumn {
            item {
                Column {

                    Spacer(modifier = Modifier.height(20.dp))

                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .background(color = MaterialTheme.colors.white),
                        horizontalAlignment = Alignment.CenterHorizontally,

                        ) {
                        Row(
                            modifier = Modifier
                                .wrapContentHeight()
                                .padding(vertical = 25.dp),
                            horizontalArrangement = Arrangement.Center,
                            verticalAlignment = Alignment.CenterVertically
                        ) {


                            Box(modifier = Modifier.padding(horizontal = 20.dp, vertical = 10.dp)) {
                                CImage(
                                    model = R.drawable.ic_profile_placeholder,
                                    modifier = Modifier
                                        .size(82.dp)
                                        .clip(CircleShape)
                                        .clickable {
                                        },
                                )

                                Box(
                                    modifier = Modifier
                                        .align(Alignment.BottomEnd)
                                        .padding(3.dp)
                                ) {
                                    CheckCircle(size = 17)
                                }
                            }
                            Spacer(modifier = Modifier.height(10.dp))
                            Column(
                                modifier = Modifier
                                    .weight(1.0F)
                                    .background(color = MaterialTheme.colors.white),
                                horizontalAlignment = Alignment.Start,
                                ) {


                                Spacer(modifier = Modifier.height(5.dp))


                            }


                        }

                       // Spacer(modifier = Modifier.height(20.dp))

                    }

                    Spacer(modifier = Modifier.height(20.dp))

                    Column(modifier = Modifier.background(color = MaterialTheme.colors.white)) {
                        ItemAccount(
                            icon = R.drawable.ic_provider_info,
                            title = "User Information",
                            onTap = {
                            })

                        ItemAccount(
                            icon = R.drawable.ic_address,
                            title = "Address",
                            onTap = {
                            })

                        ItemAccount(
                            icon = R.drawable.ic_about_info,
                            title = "About Me",
                            onTap = {
                            }
                        )
                        ItemAccount(
                            icon = R.drawable.ic_email,
                            title = "Email Address",
                            onTap = {
                            }
                        )

                    }

                }
                Spacer(modifier = Modifier.height(70.dp))
            }
        }
    }
}

