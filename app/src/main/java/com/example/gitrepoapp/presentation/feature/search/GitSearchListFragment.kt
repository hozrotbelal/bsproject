@file:OptIn(ExperimentalComposeUiApi::class)

package com.example.gitrepoapp.presentation.feature.search

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.*
import androidx.compose.material.pullrefresh.PullRefreshIndicator
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.layout.ContentScale

import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.gitrepoapp.R
import com.example.gitrepoapp.data.remote.dto.search.PayloadItem
import com.example.gitrepoapp.presentation.destinations.SearchDetailsDestination
import com.example.gitrepoapp.presentation.feature.details.SearchItem

import com.example.gitrepoapp.presentation.ui.theme.*
import com.example.gitrepoapp.presentation.ui.ui_components.BackPressHandler
import com.example.gitrepoapp.presentation.ui.ui_components.CAppBar
import com.example.gitrepoapp.presentation.ui.ui_components.CImage
import com.example.gitrepoapp.presentation.ui.ui_components.CTextField
import com.example.gitrepoapp.presentation.ui.ui_components.ShimmerUserListItem
import com.example.gitrepoapp.util.UserNavGraph
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterialApi::class)
@UserNavGraph(start = true)
@Destination
@Composable

fun BSUserListFragment(
    navigator: DestinationsNavigator,
) {

    val vm = hiltViewModel<GitSearchListVM>()
    val state = vm.state.collectAsState()
    val context = LocalContext.current
    val keyBoard = LocalSoftwareKeyboardController.current
    val refreshScope = rememberCoroutineScope()

    // Filter search value
    LaunchedEffect(key1 = state.value.searchValue, block = {
        if (!state.value.isShowSearch && state.value.searchValue.isEmpty()) {
           // vm.updateShowShimmer(true)
            return@LaunchedEffect
        } else if (state.value.isShowSearch && state.value.searchValue.isEmpty()) {
            delay(800L)
            vm.resetList()
        } else {
            delay(1000L)
            vm.resetList()
        }

    })

    // is Pull To Refresh
    LaunchedEffect(state.value.isPullToRefresh) {
        if (state.value.isPullToRefresh) {
            delay(1000L)
            vm.resetList()
            vm.updatePullToRefresh(false)
        }
    }

    // onBack Pressed
    BackPressHandler(onBackPressed = {
        when {
            state.value.isShowSearch -> {
                vm.updateShowSearch(false)
                vm.updateSearchValue("")
                vm.resetList()
            }
            else -> {
                navigator.popBackStack()
                //  vm.resetList()
            }
        }

    })

    // Pull To Refresh
    fun pullToRefresh() = refreshScope.launch {
        vm.updatePullToRefresh(true)
        keyBoard?.hide()
        delay(600L)
        vm.resetList()
        vm.updatePullToRefresh(false)
    }

    val stateRefresh = rememberPullRefreshState(
        refreshing = state.value.isPullToRefresh,
        ::pullToRefresh,
        refreshingOffset = 100.dp,
    )

    Box(
        modifier = Modifier
    ) {
        val placeholderTextStyle =
            MaterialTheme.typography.s15w700.copy(color = MaterialTheme.colors.textGrey78)
        val valueTextStyle =
            MaterialTheme.typography.s15w700.copy(color = MaterialTheme.colors.textGray3C)
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(color = MaterialTheme.colors.background)
        ) {
            Box(
                modifier = Modifier.wrapContentHeight()
            ) {

                if (!state.value.isShowSearch)
                    CAppBar(
                        title = "Search",
                        showLeadingIcon = false,
                        trailingContent = {

                            Image(
                                modifier = Modifier.clickable {
                                    vm.updateShowSearch(true)
                                },
                                painter = painterResource(id = R.drawable.ic_search_black),
                                colorFilter = ColorFilter.tint(color = MaterialTheme.colors.icBlack),
                                contentDescription = ""
                            )
                        })

                if (state.value.isShowSearch)
                    Row(
                        horizontalArrangement = Arrangement.Center,
                        modifier = Modifier
                            .background(color = MaterialTheme.colors.white.copy(alpha = 1f))
                            .padding(paddingAppBar)
                            .padding(end = 10.dp)
                            .statusBarsPadding()
                            .height(52.dp),

                        ) {

                        Box(
                            modifier = Modifier
                                .padding(start = 12.dp, top = 7.dp)
                                .fillMaxSize()
                                .weight(1.0f)
                        ) {

                            CTextField(
                                modifier = Modifier
                                    .height(38.dp),
                                showDivider = false,
                                showLabel = false,
                                placeholder = "Search......",
                                placeholderTextStyle = placeholderTextStyle,
                                valueTextStyle = valueTextStyle,
                                imeAction = ImeAction.Done,
                                keyboardActions = KeyboardActions(
                                    onDone = {
                                        // focusManager.moveFocus(FocusDirection.Down)
                                        keyBoard?.hide()
                                        vm.resetList()
                                    }
                                ),
                                value = state.value.searchValue,
                                onValueChange = {
                                    vm.updateSearchValue(it)

                                }

                            )
                        }

                        Image(
                            modifier = Modifier
                                .padding(top = 5.dp)
                                .align(Alignment.CenterVertically)
                                .clickable {
                                    vm.updateShowSearch(false)
                                    vm.updateSearchValue("Android")
                                    vm.resetList()
                                },
                            painter = painterResource(id = R.drawable.ic_clear),
                            colorFilter = ColorFilter.tint(color = MaterialTheme.colors.icBlack),
                            contentDescription = ""
                        )
                    }

            }

            Box(
                Modifier
                    .fillMaxSize()
                    .zIndex(-1f)
                    .pullRefresh(state = stateRefresh)
            ) {
                LazyColumn(
                    contentPadding = PaddingValues(vertical = paddingDefaultScreen),
                    modifier = Modifier
                        .fillMaxWidth(),
                    content = {

                        item {
                            Row(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(
                                        start = paddingDefaultScreen,
                                        bottom = 10.dp,
                                        end = paddingDefaultScreen
                                    ),
                                horizontalArrangement = Arrangement.Center
                            ) {
                                Text(
                                    text ="GitHub List",
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .weight(1.0f),
                                    style = MaterialTheme.typography.s18w700.copy(color = MaterialTheme.colors.textBlack00)
                                )

                            }
                        }
                        if (!state.value.isPullToRefresh) {

                            itemsIndexed(state.value.items) { index, item ->
                                navigator?.let {
                                    Column(
                                        modifier = Modifier
                                            .padding(horizontal = paddingDefaultScreen)
                                            .clip(
                                                shape = RoundedCornerShape(
                                                    topEnd = if (index == 0) 12.dp else 0.dp,
                                                    topStart = if (index == 0) 12.dp else 0.dp,
                                                    bottomEnd = if (index == state.value.items.size - 1) 12.dp else 0.dp,
                                                    bottomStart = if (index == state.value.items.size - 1) 12.dp else 0.dp,
                                                ),
                                            )
                                            .fillMaxWidth()
                                            .background(color = MaterialTheme.colors.white)
                                            .padding(vertical = 5.dp)
                                            .clickable {
                                                SearchItem.itemObj = state.value.items[index]
                                                navigator.navigate(SearchDetailsDestination)

                                            }

                                    ) {
                                        ItemUserSearch(navigator = it, item = item, vm = vm)
                                        if (index < state.value.items.size - 1) {
                                            Column(
                                                modifier = Modifier
                                                    .padding(horizontal = paddingDefaultScreen)
                                                    .background(MaterialTheme.colors.white)
                                            ) {
                                                Divider(
                                                    thickness = 0.5.dp,
                                                    color = MaterialTheme.colors.dividerF2,
                                                    modifier = Modifier.padding(start = 75.dp)
                                                )
                                            }
                                        }

                                    }
                                }

                            }

                            items(6) {
                                ShimmerUserListItem(
                                    isShimmerLoading = state.value.isFirstShowShimmer,
                                    contentAfterLoading = {
                                    },
                                    modifier = Modifier
                                        .fillMaxWidth()
                                )
                            }

                            item {
                                if (!state.value.isFirstShowShimmer && state.value.pageIndex > 1 && state.value.isLoading) {
                                    Row(
                                        modifier = Modifier
                                            .fillMaxWidth()
                                            .padding(horizontal = paddingDefaultScreen)
                                            .clip(
                                                RoundedCornerShape(
                                                    bottomEnd = 12.dp,
                                                    bottomStart = 12.dp
                                                )
                                            )
                                            .background(MaterialTheme.colors.white)
                                            .padding(8.dp),
                                        horizontalArrangement = Arrangement.Center
                                    ) {
                                        CircularProgressIndicator()
                                    }
                                }
                            }
                        }
                    })

                PullRefreshIndicator(
                    state.value.isPullToRefresh,
                    stateRefresh,
                    contentColor = MaterialTheme.colors.primary,
                    modifier = Modifier
                        .padding(top = 0.dp)
                        .align(Alignment.TopCenter)
                )

            }

        }
    }
}


@Composable
fun ItemUserSearch(navigator: DestinationsNavigator, item: PayloadItem, vm: GitSearchListVM) {

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = paddingDefaultScreen)
            .background(color = MaterialTheme.colors.white)
            .padding(start = 5.dp, top = paddingDefaultScreen, bottom = paddingDefaultScreen),

        horizontalArrangement = Arrangement.SpaceBetween
    ) {

        CImage(
            modifier = Modifier
                .padding(horizontal = 2.dp)
                .clip(RoundedCornerShape(12.dp))
                .height(40.dp)
                .clip(RectangleShape),
            model = item.owner?.avatarUrl ?: "",//state.value.coverPhotoUrl,
            contentScale = ContentScale.Fit,
            error = {
                Image(
                    painter = painterResource(id = R.drawable.img_banner_placeholder),
                    contentDescription = "errorImage"
                )
            }
        )
        Spacer(modifier = Modifier.width(12.dp))
        Column(modifier = Modifier.weight(1.0f)) {
            Text(
                text = item.name ?: "",
                style = MaterialTheme.typography.s16w700.copy(color = MaterialTheme.colors.textBlack00)
            )
            Text(
                text = item.fullName ?: "",
                style = MaterialTheme.typography.s12w500.copy(color = MaterialTheme.colors.grey43)
            )

            Spacer(modifier = Modifier.height(5.dp))

            var watchersCount = "${item.watchersCount ?: ""}"
            var forksCount = "${item.forksCount ?: ""}"
            if (watchersCount.trim().isNotEmpty()) {
                watchersCount = "Watcher Count  $watchersCount"
            }

            if (forksCount.trim().isNotEmpty()) {
                watchersCount = "$watchersCount \nForks Count  $forksCount"
            }

            Text(
                text = watchersCount,
                style = MaterialTheme.typography.s12w500.copy(color = MaterialTheme.colors.grey43)
            )
        }

    }
}



