package com.example.gitrepoapp.presentation.feature.search

import com.example.gitrepoapp.data.remote.dto.search.PayloadItem

data class GitSearchListState(
    val sort:String = "stars",
    var items: List<PayloadItem> = emptyList(),
    val pageIndex: Int = 1,
    val filterExpanded: Boolean = false,
    val isLoading: Boolean = false,
    val endReached: Boolean = false,
    val searchValue: String = "Android",
    val isShowSearch: Boolean = false,
    val isPullToRefresh: Boolean = false,
    val isFirstShowShimmer: Boolean = false,
)
