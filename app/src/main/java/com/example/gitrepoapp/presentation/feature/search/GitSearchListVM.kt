package com.example.gitrepoapp.presentation.feature.search

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.gitrepoapp.data.repository.search.SearchRepository
import com.example.gitrepoapp.util.paginator.DefaultPaginator
import com.example.gitrepoapp.presentation.common_event.CommonEvent

import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class GitSearchListVM @Inject constructor(
    private val userRepository: SearchRepository,
) : ViewModel() {
    private val _state = MutableStateFlow(GitSearchListState())
    val state = _state.asStateFlow()


    private val _event = Channel<CommonEvent>()
    val event = _event.receiveAsFlow()
    var order: String = ""
    var quary: String = ""
    val sort = "stars"
    //    private val  _event = Channel<CommonEvent>()
//    val event = _event.receiveAsFlow()

    val paginator = DefaultPaginator(
        initialKey = _state.value.pageIndex,
        onLoadingUpdated = { value ->
            updateLoading(value)
        },
        onRequest = { nextPage ->
            userRepository.getGitSearchRepositories(
                pageIndex = nextPage,
                q = _state.value.searchValue,
                sort = _state.value.sort,
            )
        },
        getNextKey = {
            _state.update { it.copy(pageIndex = it.pageIndex + 1) }
            _state.value.pageIndex
        },
        onError = {
            updateShowShimmer(false)
        },
        onSuccess = { payload, newKey ->
            payload.let { list ->

                _state.update {
                    it.copy(
                        items = it.items + list,
                        pageIndex = newKey,
                        //endReached = payload.currentPage == payload.totalPages
                    )
                }
                updateShowShimmer(false)
            }
        }
    )

    fun updateLoading(value: Boolean) = _state.update { it.copy(isLoading = value) }
    fun updateSearchValue(value: String) = _state.update { it.copy(searchValue = value) }
    fun updateShowSearch(value: Boolean) = _state.update { it.copy(isShowSearch = value) }
    fun updateSortSearch(value: String) = _state.update { it.copy(sort = value) }
    fun updatePullToRefresh(value: Boolean) = _state.update { it.copy(isPullToRefresh = value) }
    fun updateShowShimmer(value: Boolean) = _state.update { it.copy(isFirstShowShimmer = value) }


    init {
        updateShowShimmer(true)
    }

    fun loadNextItems() {
        viewModelScope.launch {
            paginator.loadNextItems()
        }
    }

    fun resetList() {
        paginator.reset()
//        _state.update { it.copy(pageIndex = 0) }
        _state.update { it.copy(items = emptyList()) }
        updateShowShimmer(true)
        loadNextItems()
    }

}