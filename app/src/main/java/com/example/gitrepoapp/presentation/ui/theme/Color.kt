package com.example.gitrepoapp.presentation.ui.theme

import androidx.compose.material.Colors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

@get:Composable
val Colors.orange21: Color
    get() = if (isLight) Color(0xFFF47521) else Color(0xFFF47521)

@get:Composable
val Colors.white: Color
    get() = if (isLight) Color(0xFFFFFFFF) else Color(0xFF2B2B2B)

@get:Composable
val Colors.bgBlue: Color
    get() = if (isLight) Color(0xFF19A6F5) else Color(0xFF19A6F5)

@get:Composable
val Colors.bgBlueF8: Color
    get() = if (isLight) Color(0xFF51ADF8) else Color(0xFF51ADF8)

@get:Composable
val Colors.bgBlueFF: Color
    get() = if (isLight) Color(0xFFDAF5FF) else Color(0xFFDAF5FF)

@get:Composable
val Colors.bgBlueDB: Color
    get() = if (isLight) Color(0xFF44B4DB) else Color(0xFF44B4DB)

@get:Composable
val Colors.transparent: Color
    get() = if (isLight) Color(0x00000000) else Color(0x00000000)

@get:Composable
val Colors.bgBlack: Color
    get() = if (isLight) Color(0xFF000000) else Color(0xFF000000)

@get:Composable
val Colors.icBlack: Color
    get() = if (isLight) Color(0xFF000000) else Color(0xFF000000)


@get:Composable
val Colors.greyDB: Color
    get() = if (isLight) Color(0xFFDBDBDB) else Color(0xFFDBDBDB)

@get:Composable
val Colors.greyD0: Color
    get() = if (isLight) Color(0xFFD2D0D0) else Color(0xFFD2D0D0)

@get:Composable
val Colors.textGrey62: Color
    get() = if (isLight) Color(0xFF636262) else Color(0xFFAAAAAA)

@get:Composable
val Colors.textGrey65: Color
    get() = if (isLight) Color(0xFF656565) else Color(0xFFA2A2A2)

@get:Composable
val Colors.textOrange00: Color
    get() = if (isLight) Color(0xFFFFA000) else Color(0xFFFFA000)

@get:Composable
val Colors.textBD: Color
    get() = if (isLight) Color(0xFFBDBDBD) else Color(0xFF494949)

@get:Composable
val Colors.text85: Color
    get() = if (isLight) Color(0xFF858585) else Color(0xFF858585)

@get:Composable
val Colors.text8E: Color
    get() = if (isLight) Color(0xFF8E8E8E) else Color(0xFF8E8E8E)

@get:Composable
val Colors.text58: Color
    get() = if (isLight) Color(0xFF585858) else Color(0xFFA8A8A8)

@get:Composable
val Colors.text56: Color
    get() = if (isLight) Color(0xFF565656) else Color(0xFFAFAFAF)

@get:Composable
val Colors.dividerF2: Color
    get() = if (isLight) Color(0xFFF2F2F2) else Color(0xFF222222)

@get:Composable
val Colors.dividerEC: Color
    get() = if (isLight) Color(0xFFECECEC) else Color(0xFFECECEC)

@get:Composable
val Colors.dividerF1: Color
    get() = if (isLight) Color(0xFFF1F1F1) else Color(0xFF252525)

@get:Composable
val Colors.dividerE5: Color
    get() = if (isLight) Color(0xFFE5E5E5) else Color(0xFF272727)

@get:Composable
val Colors.dividerC5: Color
    get() = if (isLight) Color(0xFFC5C5C5) else Color(0xFF272727)

@get:Composable
val Colors.dividerA3: Color
    get() = if (isLight) Color(0xFFA1A2A3) else Color(0xFFA1A2A3)

@get:Composable
val Colors.textGrey78: Color
    get() = if (isLight) Color(0xFF787878) else Color(0xFF9C9C9C)

@get:Composable
val Colors.textGreyA6: Color
    get() = if (isLight) Color(0xFFA6A6A6) else Color(0xFF636363)

@get:Composable
val Colors.textGreyB1: Color
    get() = if (isLight) Color(0xFFB1B1B1) else Color(0xFF5F5F5F)

@get:Composable
val Colors.textGrey9F: Color
    get() = if (isLight) Color(0xFF9F9F9F) else Color(0xFF777777)

@get:Composable
val Colors.textBlack1C: Color
    get() = if (isLight) Color(0xFF1C1C1C) else Color(0xFFDFDFDF)

@get:Composable
val Colors.textBlack5C: Color
    get() = if (isLight) Color(0xFF50555C) else Color(0xFFA5AFBD)

@get:Composable
val Colors.textGrey9B: Color
    get() = if (isLight) Color(0xFF9B9B9B) else Color(0xFF7C7C7C)

@get:Composable
val Colors.textGreyA3: Color
    get() = if (isLight) Color(0xFFA1A2A3) else Color(0xFF636464)

@get:Composable
val Colors.textGrey7B: Color
    get() = if (isLight) Color(0xFF7B7B7B) else Color(0xFF969595)

@get:Composable
val Colors.textDarkGrey32: Color
    get() = if (isLight) Color(0xFF323232) else Color(0xFFCCCCCC)

@get:Composable
val Colors.textBlack00: Color
    get() = if (isLight) Color(0xFF000000) else Color(0xFFC0C0C0)

@get:Composable
val Colors.textGreyB0: Color
    get() = if (isLight) Color(0xFFB1B0B0) else Color(0xFF5E5D5D)

@get:Composable
val Colors.textGreyA7: Color
    get() = if (isLight) Color(0xFFA7A7A7) else Color(0xFF646464)

@get:Composable
val Colors.text82: Color
    get() = if (isLight) Color(0xFF838282) else Color(0xFF838282)

@get:Composable
val Colors.textGray4E: Color
    get() = if (isLight) Color(0xFF3A494E) else Color(0xFF85A6B1)

@get:Composable
val Colors.textGrayA9: Color
    get() = if (isLight) Color(0xFFABA9A9) else Color(0xFF5A5959)

@get:Composable
val Colors.textGray6C: Color
    get() = if (isLight) Color(0xFFABA9A9) else Color(0xFF6C6C6C)

@get:Composable
val Colors.textGray3C: Color
    get() = if (isLight) Color(0xFF1D323C) else Color(0xFF5691AD)

@get:Composable
val Colors.textGray86: Color
    get() = if (isLight) Color(0xFF868686) else Color(0xFF868686)

@get:Composable
val Colors.text71: Color
    get() = if (isLight) Color(0xFF777171) else Color(0xFFB8AEAE)

@get:Composable
val Colors.text5D: Color
    get() = if (isLight) Color(0xFF635D5D) else Color(0xFF635D5D)

@get:Composable
val Colors.text93: Color
    get() = if (isLight) Color(0xFF939393) else Color(0xFF939393)

@get:Composable
val Colors.text73: Color
    get() = if (isLight) Color(0xFF737373) else Color(0xFF737373)

@get:Composable
val Colors.textC4: Color
    get() = if (isLight) Color(0xFFC4C4C4) else Color(0xFF5A5A5A)

@get:Composable
val Colors.textAC: Color
    get() = if (isLight) Color(0xFFACACAC) else Color(0xFFACACAC)

@get:Composable
val Colors.textGrayA1: Color
    get() = if (isLight) Color(0xFFA1A1A1) else Color(0xFFA1A1A1)

@get:Composable
val Colors.textGray97: Color
    get() = if (isLight) Color(0xFF979797) else Color(0xFF808080)

@get:Composable
val Colors.text49: Color
    get() = if (isLight) Color(0xFF524B49) else Color(0xFFAA9C98)

@get:Composable
val Colors.text74: Color
    get() = if (isLight) Color(0xFF747474) else Color(0xFF747474)

@get:Composable
val Colors.textGrayA8: Color
    get() = if (isLight) Color(0xFFA8A8A8) else Color(0xFF535353)

@get:Composable
val Colors.textGray5B: Color
    get() = if (isLight) Color(0xFF5B5B5B) else Color(0xFF5B5B5B)

@get:Composable
val Colors.textLightGrey9D: Color
    get() = if (isLight) Color(0xFF9D9D9D) else Color(0xFF747474)

@get:Composable
val Colors.textGrey7D: Color
    get() = if (isLight) Color(0xFF7D7D7D) else Color(0xFFAAAAAA)

@get:Composable
val Colors.textGrey6D: Color
    get() = if (isLight) Color(0xFF6D6D6D) else Color(0xFFA3A3A3)


@get:Composable
val Colors.textBlueDF: Color
    get() = if (isLight) Color(0xFF339ADF) else Color(0xFF339ADF)

@get:Composable
val Colors.greyA7: Color
    get() = if (isLight) Color(0xFFA7A7A7) else Color(0xFF747474)

@get:Composable
val Colors.greyC8: Color
    get() = if (isLight) Color(0xFFC8C8C8) else Color(0xFFC8C8C8)

@get:Composable
val Colors.greyD9: Color
    get() = if (isLight) Color(0xFFD9D9D9) else Color(0xFFD9D9D9)

@get:Composable
val Colors.greyF8: Color
    get() = if (isLight) Color(0xFFF8F8F8) else Color(0xFF1D1D1D)

@get:Composable
val Colors.grey98: Color
    get() = if (isLight) Color(0xFF989898) else Color(0xFF989898)

@get:Composable
val Colors.grey43: Color
    get() = if (isLight) Color(0xFF3C3C43) else Color(0xFF3C3C43)

@get:Composable
val Colors.grey90: Color
    get() = if (isLight) Color(0xFFA7A7A7) else Color(0xFF909090)

@get:Composable
val Colors.bgGreyF0: Color
    get() = if (isLight) Color(0xFFF0F0F0) else Color(0xFFF0F0F0)


@get:Composable
val Colors.bg6D: Color
    get() = if (isLight) Color(0xFF43596D) else Color(0xFF43596D)


@get:Composable
val Colors.bgLightBlueDB: Color
    get() = if (isLight) Color(0xFF44B4DB) else Color(0xFF44B4DB)

@get:Composable
val Colors.bg44: Color
    get() = if (isLight) Color(0xFF444444) else Color(0xFF444444)

@get:Composable
val Colors.bgLightBlue6FF: Color
    get() = if (isLight) Color(0xFFDDF6FF) else Color(0xFFDDF6FF)


@get:Composable
val Colors.bgMarigold400: Color
    get() = if (isLight) Color(0xFFFAB400) else Color(0xFFFAB400)

@get:Composable
val Colors.bgLightMarigoldAD9: Color
    get() = if (isLight) Color(0xFFFFFAD9) else Color(0xFFFFFAD9)

@get:Composable
val Colors.bgBlueF3: Color
    get() = if (isLight) Color(0xFF4384F3) else Color(0xFF4384F3)

@get:Composable
val Colors.bgLightBlueFF: Color
    get() = if (isLight) Color(0xFFDDEAFF) else Color(0xFFDDEAFF)


@get:Composable
val Colors.bgLightBlue4FF: Color
    get() = if (isLight) Color(0xFFECF4FF) else Color(0xFFECF4FF)

@get:Composable
val Colors.bgPurpleC1: Color
    get() = if (isLight) Color(0xFF6C00C1) else Color(0xFF6C00C1)

@get:Composable
val Colors.bgLightPurpleCFF: Color
    get() = if (isLight) Color(0xFFF0DCFF) else Color(0xFFF0DCFF)

@get:Composable
val Colors.textBlue: Color
    get() = if (isLight) Color(0xFF27A2DB) else Color(0xFF165777)

@get:Composable
val Colors.textBlueF6: Color
    get() = if (isLight) Color(0xFF5BB8F6) else Color(0xFF264D68)

@get:Composable
val Colors.textBlueF5: Color
    get() = if (isLight) Color(0xFF19A6F5) else Color(0xFF0C4566)

@get:Composable
val Colors.textBlueDD: Color
    get() = if (isLight) Color(0xFF429FDD) else Color(0xFF429FDD)

@get:Composable
val Colors.textOrange: Color
    get() = if (isLight) Color(0xFFEC823F) else Color(0xFFEC823F)

@get:Composable
val Colors.orange: Color
    get() = if (isLight) Color(0xFFF47520) else Color(0xFFF47520)

@get:Composable
val Colors.orange8800: Color
    get() = if (isLight) Color(0xFFFF8800) else Color(0xFFFF8800)

@get:Composable
val Colors.textRed00: Color
    get() = if (isLight) Color(0xFFFF0000) else Color(0xFFFF0000)

@get:Composable
val Colors.red: Color
    get() = if (isLight) Color(0xFFFF0000) else Color(0xFFFF0000)

@get:Composable
val Colors.red22: Color
    get() = if (isLight) Color(0xFFFF0022) else Color(0xFFFF0022)

@get:Composable
val Colors.lightRedF1: Color
    get() = if (isLight) Color(0xFFFFF1F1) else Color(0xFF242222)

@get:Composable
val Colors.green14: Color
    get() = if (isLight) Color(0xFF02BE14) else Color(0xFF00850C)

@get:Composable
val Colors.textGreen0C: Color
    get() = if (isLight) Color(0xFF84CB0C) else Color(0xFF84CB0C)

@get:Composable
val Colors.textAF: Color
    get() = if (isLight) Color(0xFF0FCDAF) else Color(0xFF0FCDAF)

@get:Composable
val Colors.green4B: Color
    get() = if (isLight) Color(0xFF20C74B) else Color(0xFF177930)

@get:Composable
val Colors.textGreen37: Color
    get() = if (isLight) Color(0xFF00C537) else Color(0xFF007A22)

@get:Composable
val Colors.textGreen52: Color
    get() = if (isLight) Color(0xFF34A852) else Color(0xFF34A852)

@get:Composable
val Colors.green52: Color
    get() = if (isLight) Color(0xFF34A852) else Color(0xFF34A852)

@get:Composable
val Colors.backgroundF3: Color
    get() = if (isLight) Color(0xFFF3F3F3) else Color(0xFF242424)

@get:Composable
val Colors.chart99FF: Color
    get() = if (isLight) Color(0xFF0099FF) else Color(0xFF0099FF)

@get:Composable
val Colors.chartE0FF: Color
    get() = if (isLight) Color(0xFF00E0FF) else Color(0xFF00E0FF)

@get:Composable
val Colors.chart8800: Color
    get() = if (isLight) Color(0xFFFF8800) else Color(0xFFFF8800)

@get:Composable
val Colors.chartDADA: Color
    get() = if (isLight) Color(0xFFDADADA) else Color(0xFFDADADA)

@get:Composable
val Colors.chart2020: Color
    get() = if (isLight) Color(0xFFFF2020) else Color(0xFFFF2020)

@get:Composable
val Colors.chart79A7: Color
    get() = if (isLight) Color(0xFF5579A7) else Color(0xFF5579A7)

@get:Composable
val Colors.chart56A3: Color
    get() = if (isLight) Color(0xFF9056A3) else Color(0xFF9056A3)

@get:Composable
val Colors.chartA765: Color
    get() = if (isLight) Color(0xFF41A765) else Color(0xFF41A765)

@get:Composable
val Colors.chart8C1C: Color
    get() = if (isLight) Color(0xFFF58C1C) else Color(0xFFF58C1C)


@get:Composable
val Colors.linearE8: Color
    get() = if (isLight) Color(0xFFF1F1F1) else Color(0xFFFEF4E8)

@get:Composable
val Colors.linearF4: Color
    get() = if (isLight) Color(0xFFF4EEF4) else Color(0xFFF4EEF4)

@get:Composable
val Colors.linearF7: Color
    get() = if (isLight) Color(0xFFEDF1F7) else Color(0xFFEDF1F7)

@get:Composable
val Colors.linearA5: Color
    get() = if (isLight) Color(0xFF5377A5) else Color(0xFF5377A5)

@get:Composable
val Colors.linear64: Color
    get() = if (isLight) Color(0xFF41A664) else Color(0xFF41A664)

@get:Composable
val Colors.linearA6: Color
    get() = if (isLight) Color(0xFF5478A6) else Color(0xFF5478A6)

@get:Composable
val Colors.linearA2: Color
    get() = if (isLight) Color(0xFF8F55A2) else Color(0xFF8F55A2)

@get:Composable
val Colors.linearED: Color
    get() = if (isLight) Color(0xFFEAF6ED) else Color(0xFFEAF6ED)

@get:Composable
val Colors.rbBorder52: Color
    get() = if (isLight) Color(0xFF34A852) else Color(0xFF34A852)

@get:Composable
val Colors.boxBgF1: Color
    get() = if (isLight) Color(0xFFF1F1F1) else Color(0xFFF1F1F1)

@get:Composable
val Colors.bg6F: Color
    get() = if (isLight) Color(0xFF78F06F) else Color(0xFF78F06F)

@get:Composable
val Colors.bg95: Color
    get() = if (isLight) Color(0xFFFFB495) else Color(0xFFFFB495)

@get:Composable
val Colors.bgE5: Color
    get() = if (isLight) Color(0xFFFDF0E5) else Color(0xFFFDF0E5)


@get:Composable
val Colors.bgFF: Color
    get() = if (isLight) Color(0xFFBAA0FF) else Color(0xFFBAA0FF)

@get:Composable
val Colors.bg4FF: Color
    get() = if (isLight) Color(0xFF88D4FF) else Color(0xFF88D4FF)

@get:Composable
val Colors.bgF9: Color
    get() = if (isLight) Color(0xFFF9F9F9) else Color(0xFFF9F9F9)

@get:Composable
val Colors.bgC4: Color
    get() = if (isLight) Color(0xFFC4C4C4) else Color(0xFF5A5A5A)

@get:Composable
val Colors.borderGreyE9: Color
    get() = if (isLight) Color(0xFFE9E9E9) else Color(0xFFE9E9E9)

@get:Composable
val Colors.icon73: Color
    get() = if (isLight) Color(0xFF737373) else Color(0xFF737373)

@get:Composable
val Colors.iconYellow11: Color
    get() = if (isLight) Color(0xFFFFBA11) else Color(0xFFFFBA11)

@get:Composable
val Colors.otpGreyLight: Color
    get() = if (isLight) Color(0xFFDBDBDB) else Color(0xFFDBDBDB)

@get:Composable
val Colors.otpGreyDark: Color
    get() = if (isLight) Color(0xFF60626C) else Color(0xFF60626C)


@get:Composable
val Colors.bgGreyC4: Color
    get() = if (isLight) Color(0xFFC8C4C4) else Color(0xFFC8C4C4)

@get:Composable
val Colors.bgGreyE2: Color
    get() = if (isLight) Color(0xFFE2E2E2) else Color(0xFFE2E2E2)


@get:Composable
val Colors.bgLinenE9: Color
    get() = if (isLight) Color(0xFFFFF5E9) else Color(0xFFFFF5E9)

@get:Composable
val Colors.bgAliceBlueEF: Color
    get() = if (isLight) Color(0xFFEEF8FF) else Color(0xFFEEF8FF)

@get:Composable
val Colors.bgLightGreenE1: Color
    get() = if (isLight) Color(0xFFD7FFE1) else Color(0xFFD7FFE1)

@get:Composable
val Colors.bgEntriesF1E0: Color
    get() = if (isLight) Color(0xFFFFF1E0) else Color(0xFFFFF1E0)

@get:Composable
val Colors.bgEntriesEBFF: Color
    get() = if (isLight) Color(0xFFCDEBFF) else Color(0xFFCDEBFF)

@get:Composable
val Colors.calenderInactiveBE: Color
    get() = if (isLight) Color(0xFFBEBEBE) else Color(0xFFBEBEBE)

@get:Composable
val Colors.icGreyC6: Color
    get() = if (isLight) Color(0xFFC6C6C6) else Color(0xFFC6C6C6)

@get:Composable
val Colors.bgLightRedCA: Color
    get() = if (isLight) Color(0xFFFBE9E7) else Color(0xFFFBE9E7)

@get:Composable
val Colors.bgSolitude6FF: Color
    get() = if (isLight) Color(0xFFE8F6FF) else Color(0xFFE8F6FF)

@get:Composable
val Colors.bgGreyED: Color
    get() = if (isLight) Color(0xFFEDEDED) else Color(0xFFEDEDED)