package com.example.gitrepoapp.presentation.ui.theme

import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.ui.unit.dp

val paddingSignupScreen = 30.dp
val paddingDefaultScreen = 20.dp

val paddingAppBar = 10.dp


//Shape
val RoundedCornerShape12 = RoundedCornerShape(12.dp)
val RoundedCornerShape10 = RoundedCornerShape(10.dp)