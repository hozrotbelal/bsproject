package com.example.gitrepoapp.presentation.ui.theme

import androidx.compose.material.Typography
import androidx.compose.runtime.Composable
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.example.gitrepoapp.R


//val fontFamilyUniNeue = FontFamily(
//    Font(R.font.uni_neue_thin, FontWeight.W100),
//    Font(R.font.uni_neue_light, FontWeight.W300),
//    Font(R.font.uni_neue_book, FontWeight.Normal),
//    Font(R.font.uni_neue_regular, FontWeight.W500),
//    Font(R.font.uni_neue_bold, FontWeight.Bold),
//    Font(R.font.uni_neue_heavy, FontWeight.W800),
//    Font(R.font.uni_neue_black, FontWeight.W900),
//)

val defaultFontFamily = FontFamily(
    Font(R.font.opensans_light, FontWeight.W100),
    Font(R.font.opensans_light, FontWeight.W300),
    Font(R.font.opensans_regular, FontWeight.Normal),
    Font(R.font.opensans_medium, FontWeight.W500),
    Font(R.font.opensans_medium, FontWeight.W600),
    Font(R.font.opensans_bold, FontWeight.Bold),
    Font(R.font.opensans_extra_bold, FontWeight.W800),
    Font(R.font.opensans_extra_bold, FontWeight.W900),
)

// Set of Material typography styles to start with
val Typography = Typography(
    button = TextStyle(
        fontFamily = defaultFontFamily,
        fontWeight = FontWeight.W700,
        fontSize = 14.sp
    ),
)

@get:Composable
val Typography.s8w800: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W800,
        fontSize = 8.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s8w700: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W700,
        fontSize = 8.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s8w400: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W400,
        fontSize = 8.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s9w400: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W400,
        fontSize = 9.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s9w400Italic: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W400,
        fontSize = 9.sp,
        fontFamily = defaultFontFamily,
        fontStyle = FontStyle.Italic
    )

@get:Composable
val Typography.s9w700: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W700,
        fontSize = 9.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s10w400: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W400,
        fontSize = 10.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s10w500: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W500,
        fontSize = 10.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s10w700: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W700,
        fontSize = 10.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s10w900Italic: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W900,
        fontSize = 10.sp,
        fontStyle = FontStyle.Italic,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s11w400: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W400,
        fontSize = 11.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s11w500: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W500,
        fontSize = 11.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s11w600: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W600,
        fontSize = 11.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s11w700: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W700,
        fontSize = 11.sp,
        fontFamily = defaultFontFamily
    )


@get:Composable
val Typography.s12w700: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W700,
        fontSize = 12.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s12w500: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W500,
        fontSize = 12.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s12w400: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W400,
        fontSize = 12.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s13w400: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W400,
        fontSize = 13.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s13w500: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W500,
        fontSize = 13.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s13w700: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W700,
        fontSize = 13.sp,
        fontFamily = defaultFontFamily
    )


@get:Composable
val Typography.s14w500: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W500,
        fontSize = 14.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s14w400: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W400,
        fontSize = 14.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s14w700: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W700,
        fontSize = 14.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s15w400: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W400,
        fontSize = 15.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s15w500: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W500,
        fontSize = 15.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s15w700: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W700,
        fontSize = 15.sp,
        fontFamily = defaultFontFamily
    )


@get:Composable
val Typography.s16w400: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W400,
        fontSize = 16.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s16w500: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W500,
        fontSize = 16.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s16w600: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W600,
        fontSize = 16.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s16w700: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W700,
        fontSize = 16.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s18w700: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W700,
        fontSize = 18.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s18w500: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W500,
        fontSize = 18.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s20w700: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W700,
        fontSize = 20.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s20w900: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W900,
        fontSize = 20.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s21w500: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W500,
        fontSize = 21.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s21w700: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W700,
        fontSize = 21.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s22w400: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W400,
        fontSize = 22.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s22w700: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W700,
        fontSize = 22.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s24w700: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W700,
        fontSize = 24.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s26w800: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W800,
        fontSize = 26.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s26w700: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W700,
        fontSize = 26.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s28w700: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W700,
        fontSize = 28.sp,
        fontFamily = defaultFontFamily
    )


@get:Composable
val Typography.s30w500: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W500,
        fontSize = 30.sp,
        fontFamily = defaultFontFamily
    )


@get:Composable
val Typography.s32w700: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W700,
        fontSize = 32.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s36w700: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W700,
        fontSize = 36.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s45w700: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W700,
        fontSize = 45.sp,
        fontFamily = defaultFontFamily
    )

@get:Composable
val Typography.s56w700: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W700,
        fontSize = 56.sp,
        fontFamily = defaultFontFamily
    )


@get:Composable
val Typography.s70w700: TextStyle
    get() = TextStyle(
        fontWeight = FontWeight.W700,
        fontSize = 70.sp,
        fontFamily = defaultFontFamily
    )
