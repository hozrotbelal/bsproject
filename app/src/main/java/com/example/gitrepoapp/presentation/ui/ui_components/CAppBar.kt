package com.example.gitrepoapp.presentation.ui.ui_components

import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.example.gitrepoapp.presentation.ui.theme.paddingAppBar
import com.example.gitrepoapp.presentation.ui.theme.s20w700
import com.example.gitrepoapp.presentation.ui.theme.textBlack1C
import com.example.gitrepoapp.presentation.ui.theme.white
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import com.example.gitrepoapp.R


@Composable
fun CAppBar(
    navigator: DestinationsNavigator? = null,
    title: String = "Title",
    @DrawableRes leadingIcon: Int = R.drawable.ic_left_arrow,
    showLeadingIcon: Boolean = false,
    onLeadingIconTap: (() -> Unit)? = null,
    backgroundAlpha: Float = 0.7f,
    bgColor: Color = MaterialTheme.colors.white,
    height: Dp = 52.dp,
    leadingContent: @Composable () -> Unit = {},
    trailingContent: @Composable () -> Unit = {},
) {

    Card(elevation = 0.dp) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .background(color = bgColor.copy(alpha = backgroundAlpha))
                .padding(paddingAppBar)
                .padding(end = 10.dp)
                .statusBarsPadding()
                .height(height)
        ) {
            if (showLeadingIcon) IconButton(onClick = { if (onLeadingIconTap != null) onLeadingIconTap() else navigator?.popBackStack() }) {
                Image(
                    painter = painterResource(id = leadingIcon),
                    contentDescription = "arrow"
                )
            }
            Spacer(modifier = Modifier.width(paddingAppBar))
            if (title.trim().isNotEmpty()) {
                Text(
                    text = title,
                    style = MaterialTheme.typography.s20w700.copy(color = MaterialTheme.colors.textBlack1C)
                )
            }
            leadingContent()
            Spacer(modifier = Modifier.weight(1f))

            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.spacedBy(10.dp)
            ) {
                trailingContent()
            }

        }
    }
}

