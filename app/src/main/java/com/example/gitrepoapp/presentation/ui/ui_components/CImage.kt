package com.example.gitrepoapp.presentation.ui.ui_components

import androidx.compose.foundation.Image
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import coil.compose.SubcomposeAsyncImage
import coil.request.ImageRequest
import com.example.gitrepoapp.R

@Composable
fun CImage(
    modifier: Modifier = Modifier,
    model: Any?,
    error: @Composable () -> Unit = {
        Image(
            painter = painterResource(id = R.drawable.ic_profile_placeholder),
            contentDescription = "errorImage"
        )
    },
    contentDescription: String = "image",
    contentScale: ContentScale = ContentScale.Crop,
) {
    SubcomposeAsyncImage(
        model = ImageRequest.Builder(LocalContext.current)
            .data(model)
            .crossfade(true)
            .build(),
        error = {
            error()
        },
        loading = {
        },
        contentDescription = contentDescription,
        contentScale = contentScale,
        modifier = modifier,
    )
}