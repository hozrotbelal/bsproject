package com.example.gitrepoapp.presentation.ui.ui_components

import androidx.annotation.DrawableRes
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.selection.selectableGroup
import androidx.compose.foundation.selection.toggleable
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import coil.decode.SvgDecoder
import coil.request.ImageRequest
import com.example.gitrepoapp.R
import com.example.gitrepoapp.presentation.ui.theme.*
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.PagerState
import com.orhanobut.logger.Logger
import drawable.hexToColor


@Composable
fun <T> CDropDownField(
    modifier: Modifier = Modifier,
    items: List<T> = emptyList(),
    onSelect: (T) -> Unit = {},
    MenuItem: @Composable (T) -> Unit,
    Content: @Composable () -> Unit,
) {
    var expanded by remember {
        mutableStateOf(false)
    }

    Column(
        modifier = Modifier
            .then(modifier)
    ) {
        Box(modifier = Modifier.clickable {
            expanded = !expanded
        }) {
            Content()
        }
        Box(
            modifier = Modifier
                //.padding(vertical = 10.dp)
                .align(Alignment.End)
        ) {
            DropdownMenu(
                expanded = expanded,
                onDismissRequest = { expanded = false },
                modifier = Modifier
            ) {
                repeat(items.size) {
                    DropdownMenuItem(
                        onClick = {
                            onSelect(items[it])
                            expanded = false
                        }) {
                        MenuItem(items[it])
                    }
                }

            }
        }
    }
}

@Composable
fun <T> CDropDownField(
    modifier: Modifier = Modifier,
    text: String = "",
    placeholderStyle: Boolean = false,
    label: String = "",
    valueStyle: TextStyle = MaterialTheme.typography.s14w400.copy(color = if (placeholderStyle) MaterialTheme.colors.textGrey78 else MaterialTheme.colors.textBlack00),
    labelStyle: TextStyle = MaterialTheme.typography.s10w500.copy(color = MaterialTheme.colors.textGreyB0),
    showLabel: Boolean = false,
    enable: Boolean = true,
    errorText: String = "",
    @DrawableRes icon: Int = R.drawable.ic_arrow_down,
    items: List<T> = emptyList(),
    onSelect: (T) -> Unit = {},
    MenuItem: @Composable (T) -> Unit,
) {

    var expanded by remember {
        mutableStateOf(false)
    }

    Column(
        modifier = Modifier
            .background(
                color = if (enable) Color.Transparent else Color.Gray.copy(
                    alpha = 0.1f
                )
            )
            .then(modifier)
    ) {
        Box {
            Column(modifier = Modifier.clickable {
                if (enable) expanded = true
            }) {
                if (showLabel) Text(
                    text = label,
                    style = labelStyle,
                    modifier = Modifier
                )
                Text(
                    text = text.ifEmpty { "Please select" },
                    style = valueStyle,
                    modifier = Modifier.padding(
                        top = if (showLabel) 5.dp else 12.dp,
                        bottom = if (errorText.isNotBlank()) 5.dp else 12.dp
                    )
                )
                Divider(color = MaterialTheme.colors.dividerF2)
                if (errorText.isNotBlank()) {
                    Text(
                        text = errorText,
                        style = MaterialTheme.typography.s10w400.copy(color = Color(0xffFF0000)),
                        modifier = Modifier.padding(start = 0.dp, bottom = 12.dp)
                    )
                }
            }
            Image(
                painter = painterResource(id = icon),
                contentDescription = "",
                modifier = Modifier.align(
                    Alignment.CenterEnd
                )
            )
        }
        Box(
            modifier = Modifier
                //.padding(vertical = 10.dp)
                .align(Alignment.End)
        ) {
            DropdownMenu(
                expanded = expanded,
                onDismissRequest = { expanded = false },
                modifier = Modifier
            ) {
                repeat(items.size) {
                    DropdownMenuItem(onClick = {
                        onSelect(items[it])
                        expanded = false
                    }) {
                        MenuItem(items[it])
                    }
                }

            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun CButton(
    modifier: Modifier = Modifier,
    text: String = "Tap here",
    radius: Dp = 200.dp,
    height: Dp = 44.dp,
    textStyle: TextStyle = TextStyle.Default.copy(color = Color.White),
    backgroundColor: Color = Color(0xFFF47521),
    enabled: Boolean = true,
    onClick: () -> Unit = { Logger.w("Click not set") },

    ) {
    Button(
        onClick = onClick,
        enabled = enabled,
        colors = ButtonDefaults.buttonColors(backgroundColor = backgroundColor),
        shape = RoundedCornerShape(radius),
        modifier = modifier
            .fillMaxWidth()
            .height(height)
            .then(modifier)
    )
    {
        Text(text = text, style = textStyle)
    }
}


@Composable
fun CCircleButton(
    modifier: Modifier = Modifier,
    circleBoxSize: Dp = 48.dp,
    boxBgColor: Color = MaterialTheme.colors.backgroundF3,
    @DrawableRes icon: Int = R.drawable.ic_heart2,
    svgImage: String? = null,
    svgUrl: String? = null,
    svgHexColor: String? = null,
    icSize: Dp = 24.dp,
    colorFilter: ColorFilter? = null,
    content: @Composable () -> Unit = {},
    onClick: () -> Unit = { },
    horizontalPadding: Dp = 5.dp,
) {

    val context = LocalContext.current

    Column(
        modifier = Modifier
            .padding(horizontal = horizontalPadding)
            .then(modifier),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Box(
            modifier = Modifier
                .size(circleBoxSize)
                .clip(CircleShape)
                .background(color = boxBgColor)
                .clickable {
                    onClick.invoke()
                }
                .padding(if (svgImage != null) 3.dp else 0.dp),
            contentAlignment = Alignment.Center
        ) {
            if (svgImage != null) Text(text = svgImage, fontSize = 35.sp)
            if (svgUrl != null) {
                AsyncImage(
                    model = ImageRequest.Builder(context)
                        .data(svgUrl)
                        .decoderFactory(SvgDecoder.Factory())
                        .build(),
                    contentDescription = "",
                    modifier = Modifier.size(25.dp),
                    colorFilter = svgHexColor?.let { ColorFilter.tint(color = hexToColor(it)) }
                )
            }
            if (svgImage == null) Image(
                painter = painterResource(id = icon),
                contentDescription = "icon",
                modifier = Modifier.size(icSize),
                colorFilter = colorFilter
            )
        }

        content()
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultTextField(
    modifier: Modifier = Modifier,
    iconRes: Int? = null,
    placeholder: String = "Type here",
    showUnderline: Boolean = true,
) {
    var text by remember { mutableStateOf("") }

    TextField(
        value = text,
        onValueChange = { text = it },
        placeholder = {
            Text(
                text = placeholder,
                style = MaterialTheme.typography.s13w400.copy(color = MaterialTheme.colors.textDarkGrey32)
            )
        },
        leadingIcon = if (iconRes != null) {
            {
                Image(
                    painter = painterResource(id = iconRes ?: R.drawable.ic_user),
                    contentDescription = ""
                )
            }
        } else null,
        colors = TextFieldDefaults.textFieldColors(
            backgroundColor = Color.White,
            focusedIndicatorColor = if (showUnderline) MaterialTheme.colors.dividerF2 else Color.Transparent,
            unfocusedIndicatorColor = if (showUnderline) MaterialTheme.colors.dividerF2 else Color.Transparent,
            disabledIndicatorColor = if (showUnderline) MaterialTheme.colors.dividerF2 else Color.Transparent,
        ),
        modifier = modifier
            .fillMaxWidth()
            .then(modifier)
    )
}

@Composable
fun CTextFieldWithBg(
    title: String,
    placeholder: String,
    value: String,
    error: String = "",
    disabled: Boolean = false,
    withBg: Boolean = true,
    showDropdown: Boolean = false,
    showDivider: Boolean = false,
    keyboardType: KeyboardType = KeyboardType.Text,
    keyboardOnDone: () -> Unit,
    onValueChange: (String) -> Unit,
) {

    val placeholderTextStyle =
        MaterialTheme.typography.s14w700.copy(color = MaterialTheme.colors.textGrey78)
    val valueTextStyle153C =
        MaterialTheme.typography.s15w700.copy(color = MaterialTheme.colors.textGray3C)
    val labelTextStyle =
        MaterialTheme.typography.s12w500.copy(color = MaterialTheme.colors.text71)

    Column {
        Box {

            val modifier = if (withBg) Modifier
                .border(
                    BorderStroke(
                        1.dp,
                        color = MaterialTheme.colors.bgF9
                    )
                )
                .background(color = MaterialTheme.colors.bgF9)
                .padding(start = 12.dp, end = 10.dp, top = 8.dp, bottom = 4.dp)
            else
                Modifier.padding(
                    top = 8.dp,
                    bottom = 4.dp
                )

            Column(
                modifier = modifier,
            ) {
                Text(
                    text = title,
                    style = MaterialTheme.typography.s12w500.copy(
                        MaterialTheme.colors.textGreyB0
                    ),
                )
                CTextField(
                    showDivider = showDivider,
                    singleLine = false,
                    enable = !disabled,
                    disabledBgColor = Color.Transparent,
                    placeholder = placeholder,
                    textAlignment = Alignment.TopStart,
                    topPaddingForNotLabel = 5.dp,
                    bottomPadding = 5.dp,
                    placeholderTextStyle = placeholderTextStyle,
                    labelTextStyle = labelTextStyle,
                    valueTextStyle = valueTextStyle153C,
                    keyboardType = keyboardType,
                    imeAction = ImeAction.Done,
                    keyboardActions = KeyboardActions(
                        onDone = {
                            keyboardOnDone()
                        }
                    ),
                    value = value,
                    //                            errorText = state.value.eventNoteError,
                    onValueChange = onValueChange
                )

            }
            if (showDropdown) Image(
                painter = painterResource(id = R.drawable.ic_arrow_down),
                contentDescription = "image",
                modifier = Modifier
                    .align(
                        Alignment.CenterEnd
                    )
                    .padding(15.dp)
            )
        }
        if (error.isNotBlank()) {
            Text(
                text = error,
                style = MaterialTheme.typography.s10w400.copy(color = Color(0xffFF0000)),
                modifier = Modifier.padding(top = 3.dp)
            )
        }
    }
}

@Preview
@Composable
fun CTextField(
    modifier: Modifier = Modifier,
    iconRes: Int? = null,
    colorFilter: ColorFilter? = null,
    passwordVisible: Boolean? = true,
    keyboardType: KeyboardType = KeyboardType.Text,
    keyboardActions: KeyboardActions = KeyboardActions { },
    imeAction: ImeAction = ImeAction.Done,
    value: String = "",
    placeholder: String = "Type here",
    label: String = "Label",
    textAlignment: Alignment = Alignment.CenterStart,
    errorText: String = "",
    secondaryText: String = "",
    showLabel: Boolean = false,
    showDivider: Boolean = true,
    singleLine: Boolean = true,
    enable: Boolean = true,
    bottomPadding: Dp = 10.dp,
    topPaddingForNotLabel: Dp = 10.dp,
    disabledBgColor: Color = Color.Gray.copy(alpha = 0.1f),
    placeholderTextStyle: TextStyle = MaterialTheme.typography.s14w400.copy(color = MaterialTheme.colors.textGrey78),
    valueTextStyle: TextStyle = MaterialTheme.typography.s14w400.copy(color = MaterialTheme.colors.textDarkGrey32),
    labelTextStyle: TextStyle = MaterialTheme.typography.s10w500.copy(color = MaterialTheme.colors.textGreyB0),
    onValueChange: (String) -> Unit = { },
) {


    Column {
        if (showLabel) Text(
            text = label,
            style = labelTextStyle,
            modifier = Modifier.padding(bottom = 0.dp)
        )
        BasicTextField(
            cursorBrush = SolidColor(MaterialTheme.colors.textDarkGrey32.copy(alpha = 0.6f)),
            value = value,
            singleLine = singleLine,
            visualTransformation = if (passwordVisible!!) VisualTransformation.None else PasswordVisualTransformation(),
            onValueChange = onValueChange,
            keyboardOptions = KeyboardOptions(keyboardType = keyboardType, imeAction = imeAction),
            keyboardActions = keyboardActions,
            textStyle = valueTextStyle,
            enabled = enable,
            decorationBox = { innerTextField ->

                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(color = Color.Transparent),
                    contentAlignment = textAlignment,
                ) {

                    Row(
                        modifier = Modifier
                            .fillMaxWidth(),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        if (iconRes != null) {
                            Image(
                                painter = painterResource(id = iconRes ?: R.drawable.ic_user),
                                contentDescription = "",
                                modifier = Modifier.padding(end = 16.dp),
                                        colorFilter = colorFilter
                            )
                        }
                        if (value.isEmpty()) {
                            Text(
                                text = placeholder,
                                style = placeholderTextStyle,

                                )
                        }
                    }

                    Box(modifier = Modifier.padding(start = if (iconRes != null) 33.dp else 0.dp)) {
                        innerTextField()
                    }
                }

            },
            modifier = Modifier
                .fillMaxWidth()
                .background(color = if (enable) Color.Transparent else disabledBgColor)
//                .height()
//                .border(width = 1.dp, color = MaterialTheme.colors.greyC8)
                .padding(
                    bottom = bottomPadding,
                    top = if (showLabel) 7.dp else topPaddingForNotLabel
                )
                .then(modifier)

        )

        if (showDivider) Divider(
            color = MaterialTheme.colors.dividerF2,
            thickness = 1.dp,
            modifier = Modifier
        )

        if (errorText.isNotBlank()) {
            Text(
                text = errorText,
                style = MaterialTheme.typography.s10w400.copy(color = Color(0xffFF0000)),
                modifier = Modifier.padding(start = if (iconRes != null) 33.dp else 0.dp)
            )
        }
        if (secondaryText.isNotBlank()) {
            Text(
                text = secondaryText,
                style = MaterialTheme.typography.s10w400.copy(color = Color(0xff777171)),
                modifier = Modifier.padding(start = if (iconRes != null) 33.dp else 0.dp)
            )
        }
    }
}


@Preview
@Composable
fun CheckCircle(size: Int = 19) {
    Box(
        modifier = Modifier
            .clip(CircleShape)
            .size(size.dp),

        contentAlignment = Alignment.Center,
    ) {
        Box(
            modifier = Modifier
                .clip(CircleShape)
                .fillMaxSize()
                .background(color = MaterialTheme.colors.white)
        )
        Box(
            modifier = Modifier
                .padding((size * 0.12).dp)
                .clip(CircleShape)
                .fillMaxSize()
                .background(color = MaterialTheme.colors.green14)
        )

        Image(
            painter = painterResource(id = R.drawable.ic_check),
            contentDescription = "image",
            modifier = Modifier
                .size((size * 0.5).dp)
                .offset(y = 0.5.dp)
//                .clip(CircleShape)
//                .fillMaxSize()
//                .padding(3.dp)
        )
    }
}


@Composable
fun CCheckboxTitle(
    modifier: Modifier = Modifier,
    mainModifier: Modifier = Modifier,
    showLabel: Boolean = false,
    label: String = "Label",
    labelTextStyle: TextStyle = MaterialTheme.typography.s10w500.copy(color = MaterialTheme.colors.textGray97),
    cbSize: Int = 18,
    cbTitle: String = "",
    cbTextStyle: TextStyle = MaterialTheme.typography.s14w700.copy(color = MaterialTheme.colors.text71),
    isChecked: Boolean = false,
    onChange: (Boolean) -> Unit = {},
    cbColors: CheckboxColors = CheckboxDefaults.colors(Color(0xffC8C8C8)),
    withSpace: Dp = 8.dp,
) {
    Column(modifier = mainModifier) {
        if (showLabel) Text(
            text = label,
            style = labelTextStyle,
            modifier = modifier
        )

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .toggleable(
                    value = isChecked,
                    onValueChange = onChange
                ),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Spacer(modifier = Modifier.width(5.dp))
            Checkbox(
                modifier = Modifier
                    .absoluteOffset((-1).dp, 0.dp)
                    .size(cbSize.dp),
                checked = isChecked,

                colors = cbColors,
                onCheckedChange = onChange,
            )
            Spacer(modifier = Modifier.width(withSpace))
            Text(
                text = cbTitle,
                style = cbTextStyle,
                modifier = Modifier
                    .padding(horizontal = 2.dp)

            )
        }
    }
}

@Composable
fun <T> CRadioGroup(
    mItems: List<T>,
    selectedItem: String,
    setSelected: (selected: String) -> Unit = {},
    radioGroupEnable: Boolean = true,
    rbTextStyle: TextStyle = MaterialTheme.typography.s13w400.copy(color = MaterialTheme.colors.textGrey9B),
    horizontalPadding: Int = 10,
    @DrawableRes iconSelected: Int = R.drawable.ic_selected_rb_blue,
    @DrawableRes iconUnSelected: Int = R.drawable.ic_unselected_ellipse,
) {
    CompositionLocalProvider(LocalLayoutDirection provides LayoutDirection.Ltr) {
        Column(
            modifier = Modifier.selectableGroup()
        ) {

            mItems.forEach { item ->
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(35.dp)
                        .selectable(
                            enabled = radioGroupEnable,
                            selected = (selectedItem == item),
                            onClick = { setSelected(item.toString()) },
                            role = Role.RadioButton
                        )
                        .padding(horizontal = horizontalPadding.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Image(
                        painter = painterResource(id = if (selectedItem == item) iconSelected else iconUnSelected),
                        contentDescription = "",
                        modifier = Modifier
                            .padding(end = 10.dp)
                            .size(16.dp),
                    )
                    Text(
                        modifier = Modifier.padding(start = 8.dp),
                        text = item.toString(),
                        style = rbTextStyle
                    )
                }

            }
        }
    }
}

@OptIn(ExperimentalPagerApi::class)
@Composable
fun CustomPagerIndicator(
    modifier: Modifier = Modifier,
    pagerState: PagerState,
    activeColor: Color = MaterialTheme.colors.white,
    inactiveColor: Color = MaterialTheme.colors.white,
) {

    val MULTIPLIER_SELECTED_PAGE = 2
    val baseWidth = 10.dp
    val spacing = 10.dp
    val height = 10.dp

    Row(modifier = modifier) {
        val offsetIntPart = pagerState.currentPageOffset.toInt()
        val offsetFractionalPart = pagerState.currentPageOffset - offsetIntPart
        val currentPage = pagerState.currentPage + offsetIntPart
        val targetPage = if (pagerState.currentPageOffset < 0) currentPage - 1 else currentPage + 1
        val currentPageWidth =
            baseWidth * (1 + (1 - kotlin.math.abs(offsetFractionalPart)) * MULTIPLIER_SELECTED_PAGE)
        val targetPageWidth =
            baseWidth * (1 + kotlin.math.abs(offsetFractionalPart) * MULTIPLIER_SELECTED_PAGE)

        repeat(pagerState.pageCount) { index ->
            val width = when (index) {
                currentPage -> currentPageWidth
                targetPage -> targetPageWidth
                else -> baseWidth
            }
            Box(
                modifier = Modifier
                    .clip(RoundedCornerShape(40.dp))
                    .width(width)
                    .background(if (index != pagerState.currentPage) inactiveColor else activeColor)
                    .height(height)
            )
            if (index != pagerState.pageCount - 1) {
                Spacer(modifier = Modifier.width(spacing))
            }
        }
    }
}

@Composable
fun CustomToggleButton(
    selected: Boolean,
    modifier: Modifier = Modifier,
    enabledColor: Color = MaterialTheme.colors.primary,
    disabledColor: Color = MaterialTheme.colors.greyC8,
    circleShapeSize: Dp = 13.dp,
    onUpdate: (Boolean) -> Unit
) {

    Card(
        modifier = modifier
            .width(33.dp)
            .clickable {
                onUpdate(!selected)
            }, shape = RoundedCornerShape(20.dp), elevation = 0.dp
    ) {
        Box(
            modifier = Modifier.background(
                if (selected) enabledColor else disabledColor
            ), contentAlignment = if (selected) Alignment.TopEnd else Alignment.TopStart
        ) {
            Card(
                shape = CircleShape, modifier = Modifier
                    .padding(3.dp)
                    .size(circleShapeSize), elevation = 0.dp
            ) {
                Box(modifier = Modifier.background(Color.White))
            }

        }
    }

}