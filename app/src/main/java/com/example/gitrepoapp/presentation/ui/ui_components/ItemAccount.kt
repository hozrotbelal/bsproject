package com.example.gitrepoapp.presentation.ui.ui_components

import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.gitrepoapp.presentation.ui.theme.*
import com.example.gitrepoapp.R

@Preview(showBackground = true)
@Composable
fun ItemAccount(
    @DrawableRes icon: Int = R.drawable.ic_coffee,
    title: String = "Event List",
    subTitle: String = "Subtitle",
    showSubTitle: Boolean = false,
    trailingText: String = "English",
    showTrailingText: Boolean = false,
    hideDivider: Boolean = false,
    trailingContent: @Composable() () -> Unit = {},
    onTap: (() -> Unit)? = null,
) {
    Column(modifier = Modifier.clickable {
        onTap?.let { it() }
    }) {
        Row(
            modifier = Modifier.padding(start = 20.dp, end = 15.dp, bottom = 8.dp, top = 8.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Box(
                modifier = Modifier
                    .size(36.dp)
                    .clip(CircleShape)
                    .background(color = MaterialTheme.colors.backgroundF3),
                contentAlignment = Alignment.Center
            ) {
                Image(painter = painterResource(id = icon), contentDescription = "icon")
            }
            Spacer(modifier = Modifier.width(20.dp))
            Column {
                Text(
                    text = title,
                    style = MaterialTheme.typography.s14w700.copy(color = MaterialTheme.colors.textGray4E)
                )
                if (showSubTitle) {
                    Text(
                        text = subTitle,
                        style = MaterialTheme.typography.s12w400.copy(color = MaterialTheme.colors.textGrayA8)
                    )
                }
            }
            Spacer(modifier = Modifier.weight(1f))

            Box(modifier = Modifier.padding(5.dp)) {
                if (showTrailingText) {
                    if(trailingText.trim().isNotEmpty()) {
                        Text(
                            text = trailingText,
                            modifier = Modifier.padding(end = 8.dp),
                            style = MaterialTheme.typography.s14w700.copy(color = MaterialTheme.colors.textGray4E)
                        )
                    }
                    trailingContent.invoke()
                } else {
                    Image(
                        painter = painterResource(id = R.drawable.ic_arrow_right),
                        contentDescription = "icon"
                    )
                }
            }

        }
        if (!hideDivider) Divider(
            color = MaterialTheme.colors.dividerF2,
            modifier = Modifier.padding(start = 80.dp)
        )
    }
}