package com.example.gitrepoapp.presentation.ui.ui_components

import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import com.example.gitrepoapp.presentation.ui.theme.borderGreyE9
import com.example.gitrepoapp.presentation.ui.theme.boxBgF1
import com.example.gitrepoapp.presentation.ui.theme.paddingDefaultScreen
import com.example.gitrepoapp.presentation.ui.theme.white

@Composable
fun ShimmerUserListItem(
    isShimmerLoading: Boolean = true,
    contentAfterLoading: @Composable () -> Unit = { },
    modifier: Modifier = Modifier
) {
    if (isShimmerLoading) {
        Row(modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = paddingDefaultScreen)
            .background(color = MaterialTheme.colors.white)
            .padding(start = 16.dp, top = paddingDefaultScreen, bottom = paddingDefaultScreen)
            .then(modifier)
        ) {

            Box(
                modifier = Modifier
                    .size(58.dp)
                    .clip(CircleShape)
                    .shimmerEffect()
            )
            Spacer(modifier = Modifier.width(12.dp))
            Column(
                modifier = Modifier
                    .weight(1.0f)
                    .padding(end = paddingDefaultScreen)
            ) {
                Box(
                    modifier = Modifier
                        .width(100.dp)
                        .height(20.dp)
                        .shimmerEffect()
                )
                Spacer(modifier = Modifier.height(8.dp))
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(20.dp)
                        .shimmerEffect()

                )
            }
        }

    } else {
        contentAfterLoading()
    }
}

fun Modifier.shimmerEffect(): Modifier = composed {
    var size by remember {
        mutableStateOf(IntSize.Zero)
    }
    val transition = rememberInfiniteTransition()
    val startOffsetX by transition.animateFloat(
        initialValue = -2 * size.width.toFloat(),
        targetValue = 2 * size.width.toFloat(),
        animationSpec = infiniteRepeatable(
            animation = tween(1000)
        )
    )

    background(
        brush = Brush.linearGradient(
            colors = listOf(
                MaterialTheme.colors.borderGreyE9,
                MaterialTheme.colors.boxBgF1,
                MaterialTheme.colors.borderGreyE9,

                ),
            start = Offset(startOffsetX, 0f),
            end = Offset(startOffsetX + size.width.toFloat(), size.height.toFloat())
        )
    )
        .onGloballyPositioned {
            size = it.size
        }
}

@Composable
fun ShimmerTemplateGridItem(
    isShimmerLoading: Boolean,
    modifier: Modifier = Modifier,
    contentAfterLoading: @Composable () -> Unit = {},
) {
    if (isShimmerLoading) {
        Column(
            Modifier
                .padding(5.dp)
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Box(
                modifier = modifier
                    .shimmerEffect()
            )
        }

    } else {
        contentAfterLoading()
    }
}


@Composable
fun <T> LazyListScope.gridItems(
    isShimmerLoading: Boolean,
    data: List<T>,
    columnCount: Int,
    modifier: Modifier,
    horizontalArrangement: Arrangement.Horizontal = Arrangement.Start,
    itemContent: @Composable BoxScope.(T) -> Unit,
) {
    val size = data.count()
    val rows = if (size == 0) 0 else 1 + (size - 1) / columnCount
    items(rows, key = { it.hashCode() }) { rowIndex ->
        Row(
            horizontalArrangement = horizontalArrangement,
            modifier = modifier
        ) {
            for (columnIndex in 0 until columnCount) {
                val itemIndex = rowIndex * columnCount + columnIndex
                if (itemIndex < size) {
                    Box(
                        modifier = Modifier
                            .weight(1F, fill = true)
                            .shimmerEffect(),
                        propagateMinConstraints = true
                    ) {
                        itemContent(data[itemIndex])
                    }
                } else {
                    Spacer(Modifier.weight(1F, fill = true))
                }
            }
        }
    }
}

@Composable
fun ShimmerAddressListItem(
    isShimmerLoading: Boolean,
    contentAfterLoading: @Composable () -> Unit,
    modifier: Modifier = Modifier,
    totalItem: Int = 5
) {
    if (isShimmerLoading) {
        Column(
            modifier = Modifier
                .background(color = MaterialTheme.colors.background)
                .padding(
                    top = 20.dp
                )
                .then(modifier)

        ) {
            List(totalItem) {

                Column(
                    modifier = Modifier
                        .background(color = MaterialTheme.colors.white)
                        .padding(
                            horizontal = paddingDefaultScreen,
                            vertical = 5.dp
                        )

                ) {

                    Spacer(modifier = Modifier.height(10.dp))
                    Box(
                        modifier = Modifier
                            .width(100.dp)
                            .height(20.dp)
                            .shimmerEffect()
                    )
                    Spacer(modifier = Modifier.height(4.dp))
                    Box(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(20.dp)
                            .shimmerEffect()

                    )
                    Spacer(modifier = Modifier.height(4.dp))
                }
            }
        }

    } else {
        contentAfterLoading()
    }
}


@Composable
fun ShimmerQuestionAndAnswerListItem(
    isShimmerLoading: Boolean,
    modifier: Modifier = Modifier,
    totalItem: Int = 2,
    isCircleShape: Boolean = true,
    titleWidth: Dp = 100.dp,
    boxCircleHeight : Dp = 18.dp,
    contentAfterLoading: @Composable () -> Unit = {},

    ) {
    if (isShimmerLoading) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .clip(shape = RoundedCornerShape(size = 12.dp))
                .background(color = MaterialTheme.colors.white)
                .padding(
                    start = if (isCircleShape) paddingDefaultScreen else 0.dp,
                    top = if (isCircleShape) 15.dp else 0.dp,
                    bottom = if (isCircleShape) paddingDefaultScreen else 3.dp,
                    end = 16.dp,
                )
                .then(modifier)

        ) {
            if (isCircleShape) Box(
                modifier = Modifier
                    .padding(horizontal = 10.dp)
                    .fillMaxWidth()
                    .height(20.dp)
                    .shimmerEffect()
            )
            Spacer(modifier = Modifier.height(8.dp))
            List(totalItem) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(25.dp)
                        .padding(
                            horizontal = if (isCircleShape) paddingDefaultScreen else 0.dp,
                            vertical = if (isCircleShape) 5.dp else 0.dp,
                        ),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Box(
                        modifier = Modifier
                            .width(boxCircleHeight)
                            .height(boxCircleHeight)
                            .clip(shape = if (isCircleShape) CircleShape else RectangleShape)
                            .shimmerEffect()
                    )
                    if(boxCircleHeight != 0.dp)Spacer(modifier = Modifier.width(15.dp))
                    Box(
                        modifier = Modifier
                            .width(titleWidth)
                            .height(18.dp)
                            .shimmerEffect()

                    )
                }
            }
        }

    } else {
        contentAfterLoading()
    }
}


@Composable
fun ShimmerForContactListItem(
    isShimmerLoading: Boolean,
    contentAfterLoading: @Composable () -> Unit,
    modifier: Modifier = Modifier,
    totalItem: Int = 2,
    leftWeight: Float = .7f,
    rightWeight: Float = 1f,
) {
    if (isShimmerLoading) {
        Column(
            modifier = Modifier
                .padding(horizontal = paddingDefaultScreen, vertical = 6.dp)
                .fillMaxWidth()
                .clip(shape = RoundedCornerShape(size = 12.dp))
                .background(color = MaterialTheme.colors.white)
                .padding(
                    start = 17.dp,
                    end = 16.dp,
                    bottom = 12.dp,
                )
                .then(modifier)

        ) {

            Spacer(modifier = Modifier.height(10.dp))
            List(totalItem) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(25.dp)
                        .padding(vertical = 5.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Box(
                        modifier = Modifier
                            .padding(horizontal = 10.dp)
                            .weight(leftWeight)
                            .height(20.dp)
                            .shimmerEffect()
                    )
                    Spacer(modifier = Modifier.width(15.dp))
                    Box(
                        modifier = Modifier
                            .weight(rightWeight)
                            .height(18.dp)
                            .shimmerEffect()

                    )
                }
            }
        }

    } else {
        contentAfterLoading()
    }
}
