package com.example.gitrepoapp.util

import com.example.gitrepoapp.data.remote.dto.ApiResponse
import okhttp3.Response

sealed class ApiResult<T>(
    val payload: T? = null,
    val data: T? = null,
    val body: ApiResponse<T>? = null,
    val requestResponse: Response? = null,
    val message: String = ""
) {
    class Success<T>(payload: T?,data: T?,  body: ApiResponse<T>?, requestResponse: Response? = null) : ApiResult<T>(payload = payload,data = data, body = body, requestResponse =  requestResponse)
    class Error<T>(message: String) : ApiResult<T>(message = message)
}
