package com.example.gitrepoapp.util

import android.util.DisplayMetrics
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext

import com.google.accompanist.systemuicontroller.rememberSystemUiController

import kotlin.reflect.KProperty1


@Composable
fun convertPixelsToDp(px: Int): Int {
    return px / (LocalContext.current.resources
        .displayMetrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT)
}

fun formatTime(t: Int): String {
    val hours: Int = t / 3600
    val minutes: Int = (t % 3600) / 60
    val seconds: Int = t % 60

    return "$minutes:$seconds"
}

@Composable
fun SetStatusBarColor(
    color: Color = Color.Transparent,
    useDarkIcons: Boolean = MaterialTheme.colors.isLight
) {
    val systemUiController = rememberSystemUiController()

    systemUiController.setSystemBarsColor(
        color = color,
        darkIcons = !useDarkIcons
    )
}

@Composable
fun SetNavigationBarColor(
    color: Color = Color.Transparent,
    useDarkIcons: Boolean = MaterialTheme.colors.isLight
) {
    val systemUiController = rememberSystemUiController()

    systemUiController.setNavigationBarColor(
        color = color,
        darkIcons = !useDarkIcons
    )
}


fun <T> updateListItem(
    list: List<T>,
    item: T,
    onUpdate: (T) -> T
): List<T> {
    val mutableList = list.toMutableList()
    val index = mutableList.indexOf(item)
    val objToBeUpdate = mutableList.find { it == item }
    objToBeUpdate?.let {
        val newObj = onUpdate(objToBeUpdate)
        newObj?.let { mutableList[index] = it }
    }
    return mutableList
}

fun <T> addListItem(
    list: List<T>,
    item: T
): List<T> {
    val mutableList = list.toMutableList()
    mutableList.add(item)
    return mutableList
}

fun <T> removeListItem(
    list: List<T>,
    item: T
): List<T> {
    val mutableList = list.toMutableList()
    mutableList.remove(item)
    return mutableList
}

fun getRegionCodeFromDialCode(countryCode: String?): Int {
    return try {
        countryCode?.replace(
            "+",
            ""
        )?.toInt() ?: -1
    } catch (e: Exception) {
        -1
    }
}

fun readInstanceProperty(instance: Any, propertyName: String): Any? {
    return try {
        val property = instance::class.members
            .first { it.name == propertyName } as KProperty1<Any, *>
        property.get(instance)
    } catch (e: Exception) {
        null
    }
}

fun String.replace(vararg replacements: Pair<String, String>): String {
    var result = this
    return try {
        replacements.forEach { (l, r) -> result = result.replace(l, r) }
        result
    } catch (e: Exception) {
        "--"
    }
}






