package com.example.gitrepoapp.util

import android.os.Build
import androidx.annotation.RequiresApi
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class DTUtils {

    companion object {

        const val FORMAT_24H = "HH:mm:ss"
        const val FORMAT_DDMMYYYY = "dd MMM yyyy"
        const val FORMAT_MMMDDYYYY = "MMM, dd yyyy"
        const val FORMAT_YYYY_MM_DD = "yyyy-MM-dd"
        const val FORMAT_DD_MM_YYYY = "dd-MM-yyyy"
        const val FORMAT_DD_MM_YYYY_SLASH = "dd/MM/yyyy"
        const val FORMAT_MMDDYYYY_TIME_AMPM = "MM-dd-yyyy HH:mm:ss"

        @RequiresApi(Build.VERSION_CODES.O)
        fun getLocalFormattedString(
            dateString: String?,
            format: String = FORMAT_MMDDYYYY_TIME_AMPM
        ): String {
            if (dateString.isNullOrEmpty()) return "---"
            val updatedAt = LocalDateTime.parse(dateString, DateTimeFormatter.ISO_DATE_TIME)
            val formatter = DateTimeFormatter.ofPattern(format)
            return updatedAt.format(formatter)
        }

    }
}