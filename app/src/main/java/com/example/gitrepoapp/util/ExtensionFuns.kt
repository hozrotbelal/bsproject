package com.example.gitrepoapp.util

fun Double.toDecimalPlaces(decimalPlace: Int = 1) =
    String.format("%.${decimalPlace}f", this).toDouble()


fun <T> T?.ifNull(defaultValue: () -> T): T = this ?: defaultValue()

fun Any?.isNull(): Boolean = this == null

fun Any?.isNotNull(): Boolean = this != null