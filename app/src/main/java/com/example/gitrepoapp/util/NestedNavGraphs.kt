package com.example.gitrepoapp.util

import com.ramcosta.composedestinations.annotation.NavGraph
import com.ramcosta.composedestinations.annotation.RootNavGraph


//--------------- Auth -------------------------

@RootNavGraph
@NavGraph
annotation class AuthNavGraph(
    val start: Boolean = false
)


// -------------------- Bottom bar User Nav Graph -------------------------------------
@RootNavGraph
@NavGraph
annotation class UserNavGraph(
    val start: Boolean = false
)







