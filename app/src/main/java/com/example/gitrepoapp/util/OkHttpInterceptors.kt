package com.example.gitrepoapp.util

import android.content.Context
import com.example.gitrepoapp.data.remote.common.ApiUrlConst

import com.orhanobut.logger.Logger
import dagger.hilt.android.qualifiers.ApplicationContext

import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.MediaType
import okhttp3.Protocol
import okhttp3.Request
import okhttp3.Response

import javax.inject.Inject
import javax.inject.Singleton

import okhttp3.ResponseBody.Companion.toResponseBody
import retrofit2.Invocation
import java.util.concurrent.TimeUnit

@Singleton
class HeaderInterceptor @Inject constructor(
    @ApplicationContext val context: Context,
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val original: Request = chain.request()

//        val identity = runBlocking { identityRepository.getIdentity() }
//        val token = "${identity?.tokenType} ${identity?.accessToken}"
//        Logger.d(token)

        val request: Request = original.newBuilder()
            .header("Accept", "application/json, text/plain, */*")
            .header("Origin", ApiUrlConst.BASE_URL)
           // .header("Authorization", token)
            .build()

        val response = chain.proceed(request)
        val contentType: MediaType? = response.body.contentType()
        val bodyString = response.body.string()

        return if (response.code == 401 ) {

                Response.Builder()
                    .request(chain.request())
                    .protocol(Protocol.HTTP_1_1)
                    .code(401)
                    .message("Bad Request")
                    .body("This is dummy response from interceptor when refresh token is invalid".toResponseBody())
                    .build()

        } else {
            response.newBuilder().body(bodyString.toResponseBody(contentType)).build()
        }

    }
}


@Singleton
class CacheLoggingInterceptor @Inject constructor() : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val response = chain.proceed(request)

        // Log request information
        Logger.d("Request: ${request.method} ${request.url}")
        for ((name, value) in request.headers) {
            Logger.d("$name: $value")
        }

        // Log response information
        Logger.d("Response: ${response.code} ${response.message}")
        for ((name, value) in response.headers) {
            Logger.d("$name: $value")
        }

        return response
    }
}

@Singleton
class ETagInterceptor @Inject constructor() : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        // Read the ETag header from the response
        val originalEtag = request.header("if-none-match")

        // Append "V2" to the ETag value if it's not null
        val updatedEtag = originalEtag?.let { "\"${originalEtag.replace("\"", "")}" + "V2\"" }

        // Build a new request with the updated ETag value
        val newRequest = request.newBuilder()
            .header("if-none-match", updatedEtag ?: "")
            .build()

        // Proceed with the new request
        return chain.proceed(newRequest)
    }
}


@Singleton
class CacheInterceptor @Inject constructor() : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {

        val request = chain.request()
        val builder = request.newBuilder()
        request.tag(Invocation::class.java)?.let {
            if (!it.method().isAnnotationPresent(Cacheable::class.java)) {
                builder.cacheControl(
                    CacheControl.Builder()
                        .noStore()
                        .build()
                )
                return chain.proceed(builder.build())
            }
            builder.cacheControl(
                CacheControl.Builder()
                    .maxAge(60, TimeUnit.SECONDS)
                    .build()
            )
        }
        return chain.proceed(builder.build())
    }
}
//
//@Singleton
//class ForceCacheInterceptor @Inject constructor(
//    @ApplicationContext val context: Context
//) : Interceptor {
//
//    private lateinit var connectivityObserver: ConnectivityObserver
//
//    override fun intercept(chain: Interceptor.Chain): Response {
//        connectivityObserver = NetworkConnectivityObserver(context = context)
//
//        val builder: Request.Builder = chain.request().newBuilder()
//
//        runBlocking {
//            val status = connectivityObserver.observe().first()
//            if (status == ConnectivityObserver.Status.Unavailable) {
//                builder.cacheControl(CacheControl.FORCE_CACHE);
//            }
//        }
//
//
//        return chain.proceed(builder.build());
//    }
//}