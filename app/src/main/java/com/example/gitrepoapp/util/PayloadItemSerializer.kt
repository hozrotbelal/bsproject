package com.example.gitrepoapp.util


import com.example.gitrepoapp.data.remote.dto.search.PayloadItem
import com.google.gson.Gson
import com.ramcosta.composedestinations.navargs.DestinationsNavTypeSerializer
import com.ramcosta.composedestinations.navargs.NavTypeSerializer

@NavTypeSerializer
class PayloadItemSerializer : DestinationsNavTypeSerializer<PayloadItem> {
    override fun toRouteString(value: PayloadItem): String =
        Gson().toJson(value)

    override fun fromRouteString(routeStr: String): PayloadItem =
        Gson().fromJson(routeStr, PayloadItem::class.java)
}