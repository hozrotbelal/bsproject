package com.example.gitrepoapp.util

import com.example.gitrepoapp.data.remote.dto.ApiResponse
import okhttp3.Response

sealed class Resource<T>(
    val data: T? = null,
    val payload: T? = null,
    val body: ApiResponse<T>? = null,
    val message: String? = null,
    val requestResponse: Response? = null
) {
    class Success<T>(data: T? = null, payload: T? = null,body: ApiResponse<T>? = null, requestResponse: Response? = null) :
        Resource<T>(data = data, payload = payload, body = body, requestResponse = requestResponse)
    class Error<T>(message: String, data: T? = null,payload: T? = null) : Resource<T>(data = data, payload = payload, message = message)
    class Loading<T>(val isLoading: Boolean = true) : Resource<T>(null)
}
