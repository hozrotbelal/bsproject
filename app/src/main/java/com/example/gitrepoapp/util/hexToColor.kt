package drawable

import androidx.compose.ui.graphics.Color

fun hexToColor(hexString: String): Color {
    // Remove any leading '#' symbol if present
    val cleanedHexString = if (hexString.startsWith("#")) hexString.substring(1) else hexString

    return when (cleanedHexString.length) {
        6 -> {
            // For a 6-digit hex string, parse the RGB components
            val r = cleanedHexString.substring(0, 2).toInt(16)
            val g = cleanedHexString.substring(2, 4).toInt(16)
            val b = cleanedHexString.substring(4, 6).toInt(16)
            Color(r, g, b)
        }

        8 -> {
            // For an 8-digit hex string, parse the ARGB components
            val a = cleanedHexString.substring(0, 2).toInt(16)
            val r = cleanedHexString.substring(2, 4).toInt(16)
            val g = cleanedHexString.substring(4, 6).toInt(16)
            val b = cleanedHexString.substring(6, 8).toInt(16)
            Color(a, r, g, b)
        }

        else -> {
            // Return a default color if the input is not a valid 6 or 8-digit hex string
            Color.Black
        }
    }
}