package com.example.gitrepoapp.util.paginator

import com.example.gitrepoapp.util.Resource
import kotlinx.coroutines.flow.Flow

class DefaultPaginator<Key, DataPayload>(
    private val initialKey: Key,
    private inline val onLoadingUpdated: (Boolean) -> Unit,
    private inline val onRequest: suspend (nextKey: Key) -> Flow<Resource<DataPayload>>,
    private inline val getNextKey: suspend (DataPayload) -> Key,
    private inline val onError: suspend (String?) -> Unit,
    private inline val onSuccess: suspend (payload: DataPayload, newKey: Key) -> Unit
) : Paginator<Key, DataPayload> {

    private var currentKey = initialKey
    private var isMakingRequest = false

    override suspend fun loadNextItems() {
        if (isMakingRequest) {
            return
        }
        isMakingRequest = true
        onLoadingUpdated(true)
        onRequest(currentKey).collect {
            when (it) {
                is Resource.Error -> {
                    isMakingRequest = false
                    onLoadingUpdated(false)
                    onError(it.message)
                }
                is Resource.Loading -> {
                    onLoadingUpdated(it.isLoading)
                }
                is Resource.Success -> {
                    it.data?.let { payload ->
                        currentKey = getNextKey(payload)
                        onSuccess(payload, currentKey)
                    }
                    isMakingRequest = false
                    onLoadingUpdated(false)
                }
            }
        }


    }

    override fun reset() {
        currentKey = initialKey
    }
}