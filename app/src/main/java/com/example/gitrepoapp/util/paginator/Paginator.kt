package com.example.gitrepoapp.util.paginator


interface Paginator<Key, Data> {
    suspend fun loadNextItems()
    fun reset()
}